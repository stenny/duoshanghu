<?php 
require('config.php');
$uptypes=array('image/jpg', //上传文件类型列表 
'image/jpeg', 
'image/png', 
'image/pjpeg', 
'image/gif', 
'image/bmp', 
'image/x-png'); 
$max_file_size=5000000; //上传文件大小限制, 单位BYTE 
$time2=date('Y-m');
if(isset($_REQUEST['route'])) $route=$_REQUEST['route'];
else $route='android';

$destination_folder="../Upload/".$route."/".$time2.'/'; //上传文件路径 
$destination_folder2="Upload/".$route."/".$time2.'/'; //存数据库路径 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
	if (!is_uploaded_file($_FILES["file"]['tmp_name'])) 
	//是否存在文件 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="文件不存在！";
		echo json_encode($data);
		exit; 
	} 
	$file = $_FILES["file"]; 
	// if($max_file_size < $file["size"]) 
	// //检查文件大小 
	// { 
	// 	$data[0]['pan']=-1;
	// 	$data[0]['msg']="文件太大！";
	// 	file_put_contents("tsxx_for_android.txt", "\r\n".json_encode($data)."\r\n", FILE_APPEND);
	// 	echo json_encode($data);
	// 	exit; 
	// } 
	// if(!in_array($file["type"], $uptypes)) 
	// //检查文件类型 
	// { 
	// 	$data[0]['pan']=-1;
	// 	$data[0]['msg']="只能上传图像文件！";
	// 	file_put_contents("tsxx_for_android.txt", "\r\n".json_encode($data)."\r\n", FILE_APPEND);
	// 	echo json_encode($data);
	// 	exit; 
	// } 
	if(!file_exists($destination_folder)) mkdir($destination_folder); 
	$filename=$file["tmp_name"]; 
	$image_size = getimagesize($filename); 
	$pinfo=pathinfo($file["name"]); 
	$ftype=$pinfo['extension']; 
	$destination = $destination_folder.time().".".$ftype; 
	if (file_exists($destination) && $overwrite != true) 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="同名文件已经存在了！";
		file_put_contents("tsxx_for_android.txt", "\r\n".json_encode($data)."\r\n", FILE_APPEND);
		echo json_encode($data);
		exit; 
	} 
	if(!move_uploaded_file ($filename, $destination)) 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="移动文件出错！";
		file_put_contents("tsxx_for_android.txt", "\r\n".json_encode($data)."\r\n", FILE_APPEND);
		echo json_encode($data);
		exit; 
	} 
	$pinfo=pathinfo($destination); 
	$fname=$pinfo['basename'];
	$src=$destination_folder2.$fname;
	$data[0]['pan']='1';
	$data[0]['src']=$WEB_PATH.'/'.$src;
	$data[0]['src2']=$src;
	echo json_encode($data);
} 
?>