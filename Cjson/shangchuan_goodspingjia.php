<?php 
require('config.php');
$uptypes=array('image/jpg', //上传文件类型列表 
'image/jpeg', 
'image/png', 
'image/pjpeg', 
'image/gif', 
'image/bmp', 
'image/x-png'); 
$max_file_size=5000000; //上传文件大小限制, 单位BYTE 
$time2=date('Y-m');
$destination_folder="../Upload/refund/".$time2.'/'; //上传文件路径 
$destination_folder2="Upload/refund/".$time2.'/'; //存数据库路径 
$watermark=0; //是否附加水印(1为加水印,其他为不加水印); 
$watertype=1; //水印类型(1为文字,2为图片) 
$waterposition=1; //水印位置(1为左下角,2为右下角,3为左上角,4为右上角,5为居中); 
$waterstring=""; //水印字符串 
$waterimg="xplore.gif"; //水印图片 
$imgpreview=1; //是否生成预览图(1为生成,其他为不生成); 
$imgpreviewsize=1/2; //缩略图比例 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
	if (!is_uploaded_file($_FILES["file"]['tmp_name'])) 
	//是否存在文件 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="文件不存在！";
		echo json_encode($data);
		exit; 
	} 
	$file = $_FILES["file"]; 
	if($max_file_size < $file["size"]) 
	//检查文件大小 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="文件太大！";
		echo json_encode($data);
		exit; 
	} 
	if(!in_array($file["type"], $uptypes)) 
	//检查文件类型 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="只能上传图像文件！";
		echo json_encode($data);
		exit; 
	} 
	if(!file_exists($destination_folder)) mkdir($destination_folder); 
	$filename=$file["tmp_name"]; 
	$image_size = getimagesize($filename); 
	$pinfo=pathinfo($file["name"]); 
	$ftype=$pinfo['extension']; 
	$destination = $destination_folder.time().".".$ftype; 
	if (file_exists($destination) && $overwrite != true) 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="同名文件已经存在了！";
		echo json_encode($data);
		exit; 
	} 
	if(!move_uploaded_file ($filename, $destination)) 
	{ 
		$data[0]['pan']=-1;
		$data[0]['msg']="移动文件出错！";
		echo json_encode($data);
		exit; 
	} 
	$pinfo=pathinfo($destination); 
	$fname=$pinfo['basename']; 
	$user_id=$_REQUEST['user_id'];
	$src=$destination_folder2.$fname;
	// $sql="update ".$oto."_users set userPhoto='{$src}' where userId='{$user_id}'";
	// $result=$db->query($sql);
	// if($result)
	// {
	// 	$data[0]['pan']=1;
	// 	$data[0]['msg']=$src;
	// 	echo json_encode($data);
	// }
	// else
	// {
	// 	$data[0]['pan']=-1;
	// 	$data[0]['msg']="上传失败！";
	// 	echo json_encode($data);
	// }
	$data[0]['pan']=1;
	$data[0]['msg']=$src;
	echo json_encode($data);
} 
?>