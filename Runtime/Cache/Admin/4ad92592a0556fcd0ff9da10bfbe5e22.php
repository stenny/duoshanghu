<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['shopTitle']['fieldValue']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery1.11.1.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
      <script src="/Public/plugins/formValidator/formValidator-4.1.3.js"></script>
      <script src="/Public/plugins/kindeditor/kindeditor.js"></script>
      <script src="/Public/plugins/kindeditor/lang/zh_CN.js"></script>
      <script type="text/javascript" src="/Public/js/ajaxfileupload.js"></script>
   </head>
   <script>
   $(function () {
	   KindEditor.ready(function(K) {
			editor1 = K.create('textarea[name="articleContent"]', {
				height:'350px',
				allowFileManager : false,
				allowImageUpload : true,
				items:[
				        'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
				        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
				        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
				        'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
				        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
				        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|','image','table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
				        'anchor', 'link', 'unlink', '|', 'about'
				],
				afterBlur: function(){ this.sync(); }
			});
		});
	   $.formValidator.initConfig({
		   theme:'Default',mode:'AutoTip',formID:"myform",debug:true,submitOnce:true,onSuccess:function(){
				   edit();
			       return false;
			},onError:function(msg){
		}});
	   $("#goodsName").formValidator({onFocus:"请输入商品名称"}).inputValidator({min:1,max:100,onError:"请输入100字以内商品名称"});
	   $("#shopPrice").formValidator({onFocus:"请输入为数字的商品积分"}).inputValidator({min:1,max:100,onError:"请输入商品积分"});
     $("#marketPrice").formValidator({onFocus:"请输入为数字的市场价格(元)"}).inputValidator({min:1,max:100,onError:"请输入市场价格(元)"});
     $("#goodsStock").formValidator({onFocus:"请输入为数字的库存数量"}).inputValidator({min:1,max:100,onError:"请输入商品库存"});
   });

   //使用ajaxfileupload插件异步上传图片
      jQuery(function(){   
        var url = "<?php echo U('Admin/IntegralGoods/uploadImg');?>";
          $("#buttonUpload").click(function(){
            $.ajaxFileUpload({
                url:url,//处理图片脚本
                secureuri :false,
                fileElementId :'fileToUpload1',//file控件id
                dataType : 'json',
                success : function (data, status){
                    if(data){
                      alert(data);
                    }
                },
                 error : function (data, status){
                    alert("上传错误");
                },
          });
           $.ajaxFileUpload({
                url:url,//处理图片脚本
                secureuri :false,
                fileElementId :'fileToUpload2',//file控件id
                dataType : 'json',
                success : function (data, status){
                    if(data){
                      alert(data);
                    }
                },
                 error : function (data, status){
                    alert("上传错误");
                },
          });
           $.ajaxFileUpload({
                url:url,//处理图片脚本
                secureuri :false,
                fileElementId :'fileToUpload3',//file控件id
                dataType : 'json',
                success : function (data, status){
                    if(data){
                      alert(data);
                    }
                },
                 error : function (data, status){
                    alert("上传错误");
                },
          });
          });
      })
    //保存修改
   function edit(){
	   var params = {};
	   params.id = $('#id').val();
     params.goodsCatId = $.trim($('#catId').val());
     params.goodsSn = $('#goodsSn').val();
	   params.goodsName = $('#goodsName').val();
     params.description = $('#description').val();
     params.marketPrice = $('#marketPrice').val();
     params.shopPrice = $('#shopPrice').val();
     params.goodsStock = $('#goodsStock').val();
     params.path2 = $('#file2').attr('fileId');
     params.path3 = $('#file3').attr('fileId');
     params.goodsContent = $('#goodsContent').val();
     params.isSale = $('input[name="isSale"]:checked').val();
	   if(params.goodsContent==''){
		   Plugins.Tips({title:'信息提示',icon:'error',content:'请输入商品详情!',timeout:1000});
		   return;
	   }
	   Plugins.waitTips({title:'信息提示',content:'正在提交数据，请稍后...'});
	   $.post("<?php echo U('Admin/IntegralGoods/edit');?>",params,function(data,textStatus){
			var json = WST.toJson(data);console.log(json);
			if(json.status=='1'){
				Plugins.setWaitTipsMsg({ content:'操作成功',timeout:1000,callback:function(){
				   location.href="<?php echo U('Admin/IntegralGoods/index');?>";
				}});
			}else if(json.status=='0'){
          Plugins.closeWindow();
        Plugins.Tips({title:'信息提示',icon:'error',content:'请点击上传图片!',timeout:1000});
      }else{
				Plugins.closeWindow();
				Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
			}
		});
   }
   </script>
   <body class="wst-page">
       <form name="myform" method="post" id="myform" autocomplete="off">
        <input type='hidden' id='id' value='<?php echo ($goods["goodsId"]); ?>'/>
        <table class="table table-hover table-striped table-bordered wst-form">
          <tr>
               <th width='120'>商品分类 <font color='red'>*</font>：</th>
               <td >
               <select id='catId' >
                  <option value='0'>请选择</option>
                  <?php if(is_array($goods[cats])): $i = 0; $__LIST__ = $goods[cats];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value='<?php echo ($vo['catId']); ?>' <?php if($goods['goodsCatId'] == $vo['catId'] ): ?>selected<?php endif; ?>><?php echo ($vo['catName']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
               </select>
               </td>
             </tr>
           <tr>
             <th width='120' align='right'>商品编号 <font color='red'>*</font>：</th>
             <td><input type='text' id='goodsSn' class="form-control wst-ipt" value='<?php echo ($goods["goodsSn"]); ?>' maxLength='25'/></td>
           </tr>
           <tr>
             <th width='120' align='right'>商品名称 <font color='red'>*</font>：</th>
             <td><input type='text' id='goodsName' class="form-control wst-ipt" value='<?php echo ($goods["goodsName"]); ?>' /></td>
           </tr>
           <tr>
             <th align='right'>商品描述 ：</th>
             <td><textarea name="description" id="description" class="form-control wst-ipt" onKeyUp="gbcount(this,250,'textdescription');" style="height:60px" ><?php echo ($goods["goodsDesc"]); ?></textarea>
            </td>
           </tr>
           <tr>
               <th width='120'>市场价格 <font color='red'>*</font>：</th>
               <td><input   type='text' id='marketPrice' value='<?php echo ($goods["marketPrice"]); ?>' onKeyUp="value=value.replace(/[a-z]/g,'')" maxLength='10'/></td>
             </tr>
           <tr>
             <th width='120' align='right'>兑换积分 <font color='red'>*</font>：</th>
             <td><input  type='text' id='shopPrice' value='<?php echo ($goods["shopPrice"]); ?>' onKeyUp="value=value.replace(/\D/g,'')" maxLength='50'/></td>
           </tr>
            <tr>
             <th width='120' align='right'>商品状态 <font color='red'>*</font>：</th>
             <td><input  type='radio' id='isSale' name="isSale" value='1' <?php if($goods['isSale'] == 1): ?>checked="checked"<?php endif; ?> />上架
                 <input  type='radio' id='isSale' name="isSale" value='0' <?php if($goods['isSale'] == 0): ?>checked="checked"<?php endif; ?> />下架</td>
            </tr>
          
          <tr>
               <th width='120'>商品库存 <font color='red'>*</font>：</th>
               <td><input  type='text' id='goodsStock' value='<?php echo ($goods["goodsStock"]); ?>' onKeyUp="value=value.replace(/\D/g,'')" maxLength='50'/></td>
          </tr>
          <th align="right" style="width:120px">展示图片 <font color='red'>*</font>：</th>            
          <td>
          <fieldset class="uploadpicarr">
              <div class="item">         
                <div class='content'>                 
                  缩略图<font color='red'>*</font>：<input id="fileToUpload1" type="file" size="20" name="fileToUpload1" style="display:inline" >
                  <?php if($goods["goodsImg"] != ''): ?><input id="file1" name="file1" class="form-control wst-ipt" type="text" value='<?php echo ($goods["goodsImg"]); ?>' readonly/><?php endif; ?>
                </div>          
                <div style="clear: both;"></div>
              </div>

              <div class="item">      
                <div class='content'>                 
                  图片2   &nbsp;&nbsp;：<input id="fileToUpload2" type="file" size="20" name="fileToUpload2" style="display:inline">
                  <?php if($goods[gallerys][0] != ''): ?><input id="file2" name="file2" class="form-control wst-ipt" type="text" value='<?php echo ($goods[gallerys][0][goodsImg]); ?>' fileId='<?php echo ($goods[gallerys][0][id]); ?>' readonly/><?php endif; ?>
                </div>          
                <div style="clear: both;"></div>
              </div>

              <div class="item">   
                <div class='content'>                 
                  图片3   &nbsp;&nbsp;：<input id="fileToUpload3" type="file" size="20" name="fileToUpload3" style="display:inline">
                   <?php if($goods[gallerys][1] != ''): ?><input id="file3" name="file3" class="form-control wst-ipt" type="text" value='<?php echo ($goods[gallerys][1][goodsImg]); ?>' fileId='<?php echo ($goods[gallerys][1][id]); ?>' readonly/><?php endif; ?>
                </div>          
                <div style="clear: both;"></div>
              </div>
            </fieldset><input type="button" class="btn btn-primary" id="buttonUpload" value="上传图片" ></input></td>           
          </tr>     
           <tr>
             <th align='right'>商品详情 <font color='red'>*</font>：</th>
             <td>
             <textarea id='goodsContent' name='articleContent' style='width:80%;height:400px;'><?php echo ($goods["goodsContent"]); ?></textarea>
             </td>
           </tr>
           <tr>
             <td colspan='2' style='padding-left:250px;'>
                 <button type="submit" class="btn btn-success">保&nbsp;存</button>
                 <!--
                 <button type="button" class="btn btn-primary" onclick='javascript:location.href="<?php echo U('Admin/IntegralGoods/index');?>"'>返&nbsp;回</button>
                 -->
                 <button type="button" class="btn btn-primary" onclick='history.go(-1)'>返&nbsp;回</button>
             </td>
           </tr>
        </table>
       </form>
   </body>
</html>