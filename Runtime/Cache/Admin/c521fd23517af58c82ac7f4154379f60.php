<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
   </head>

   <body class='wst-page'>
    <form method='post' action='<?php echo U("Admin/Agent/usersIndex");?>'>
       <div class='wst-tbar'>
       会员账号：<input type='text' id='loginName' name='loginName' class='form-control wst-ipt-10' value='<?php echo ($loginName); ?>'/>
       手机号码：<input type='text' id='userPhone' name='userPhone' class='form-control wst-ipt-10' value='<?php echo ($userPhone); ?>'/>
  <button type="submit" class="btn btn-primary glyphicon glyphicon-search">查询</button>
       </div>
       </form>
       <div class="wst-body">
        <table class="table table-hover table-striped table-bordered wst-list">
           <thead>
             <tr>
               <th width='30'>id</th>
               <th width='80'>账号</th>
               <th width='80'>用户名</th>
               <th width='60'>手机号码</th>
               <th width='60'>粉丝数量</th>
               <th width='60'>推荐人</th>
               <th width='60'>总佣金</th>
               <th width='60'>可提现</th>
               <th width='60'>申请中</th>
               <th width='60'>已提现</th>
               <th width='130'>注册时间</th>
               <th width='35'>状态</th>
               <th width='130'>操作</th>
             </tr>
           </thead>
           <tbody>
            <?php if(is_array($Page['root'])): $i = 0; $__LIST__ = $Page['root'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
               <td><?php echo ($vo['userId']); ?></td>
               <td><?php echo ($vo['loginName']); ?></td>
               <td><?php echo ($vo['userName']); ?>&nbsp;</td>
               <td><?php echo ($vo['userPhone']); ?>&nbsp;</td>
               <td><?php echo ($vo['fansCount']); ?>&nbsp;</td>
                 <td><?php echo ($vo['partnerId']); ?>&nbsp;</td>
                 <td><?php echo ($vo['agentTotalPrice']); ?></td>
                 <td><?php echo ($vo['agentBalance']); ?></td>
                 <td><?php echo ($vo['agentWaitPrice']); ?></td>
                 <td><?php echo ($vo['agentPayPrice']); ?></td>
               <td><?php echo ($vo['createTime']); ?>&nbsp;</td>
               <td>
               <?php if($vo['userStatus']==0 ): ?><span class='label label-danger wst-label'>
			               停用
			     </span>          
			     <?php else: ?>
			     <span class='label label-success wst-label'>
			               启用
			     </span><?php endif; ?>
               </td>
               <td>
               <?php if(in_array('hylb_02',$WST_STAFF['grant'])){ ?>
               <a class="btn btn-default glyphicon glyphicon-pencil" href="<?php echo U('Admin/agent/usersEdit',array('id'=>$vo['userId']));?>">修改</a>&nbsp;
               <?php } ?>
               <?php if(in_array('hylb_03',$WST_STAFF['grant'])){ ?>
                   <a class="btn btn-default glyphicon glyphicon-eye-open" href="<?php echo U('Admin/agent/usersMoreInfo',array('id'=>$vo['userId']));?>">粉丝详情</a>
               <!--<button type="button" class="btn btn-default glyphicon glyphicon-trash" onclick="javascript:del(<?php echo ($vo['userId']); ?>,<?php echo ($vo['userType']); ?>)">刪除</buttona>-->
               <?php } ?>
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             <tr>
                <td colspan='11' align='center'><?php echo ($Page['pager']); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
   </body>
</html>