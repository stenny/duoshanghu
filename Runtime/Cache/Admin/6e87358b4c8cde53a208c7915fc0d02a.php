<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="/Tpl/Admin/css/AdminLTE.css">
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script>
        //导出数据
        function ExportSelected(status){
          var params = {};
          params.status = status;
          var jsonText = JSON.stringify(params);
          location.href="/index.php/Admin/Stats/outExcel/data/"+jsonText;
        }
      </script>
   </head>
   <body class='wst-page'>
   
       <div class='wst-tbar' style='float:left;height:25px;'>
          <font color='red'>*</font>以下统计数据包含已删除会员的订单，订单仅为已到货状态的所有普通、团购、秒杀、积分、拍卖的有效订单
       </div>
       <div class='wst-tbar' style='text-align:right;height:25px;'>
       <?php if(in_array('spfl_01',$WST_STAFF['grant'])){ ?>
       <a class="btn btn-info glyphicon glyphicon-download-alt" href="javascript:ExportSelected(1)" style='float:right'>客户统计报表下载</a>
       <?php } ?>
       </div>
      <div class='panel wst-panel-full'>       
         <div class='row' style='padding-left:10px;margin-right:10px;'>
           <div class="col-md-12">
             <div class="box-header">
               <h4 class="text-blue">会员购买率<span style="color:#999">（会员购买率 = 有订单会员数 ÷ 会员总数）</span></h4>
               <table class="table table-hover table-striped table-bordered wst-form">
                  <tr>
                     <td width="20%" align='center'>会员总数</td>
                     <td width="30%" align='center'>有订单会员数</td>
                     <td width="20%" align='center'>会员订单总数</td>
                     <td width="30%" align='center'>会员购买率</td>
                  </tr>
                  <tr>
                     <td align='center'><?php echo ($object['sumUser']); ?></td>
                     <td align='center'><?php echo ($object['sumUsersOrder']); ?></td>
                     <td align='center'><?php echo ($object['sumUserOrders']); ?></td>
                     <td align='center'><?php echo ($object['orderRate']); ?>%</td>
                  </tr>
               </table>
             </div>
             <div class="box-header">
               <h4 class="text-blue">每会员平均订单数及购物额<span style="color:#999">（每会员订单数 = 会员订单总数 ÷ 会员总数） （每会员购物额 = 会员购物总额 ÷ 会员总数）</span></h4>
               <table class="table table-hover table-striped table-bordered wst-form">
                  <tr>
                     <td width="30%" align='center'>会员购物总额</td>
                     <td width="30%" align='center'>每会员订单数</td>
                     <td width="30%" align='center'>每会员购物额</td>
                  </tr>
                  <tr>
                     <td align='center'>￥<?php echo ($object['sumOrdersMoney']); ?></td>
                     <td align='center'><?php echo ($object['everyUserOrders']); ?></td>
                     <td align='center'>￥<?php echo ($object['everyUserMoney']); ?></td>
                  </tr>
               </table>
             </div>
          </div>
        </div>  
      </div>
   </body>
</html>