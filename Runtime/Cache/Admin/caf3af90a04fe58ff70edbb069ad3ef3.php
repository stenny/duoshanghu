<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
   </head>
   <script>
   function toggleIsShow(t,v){
	   Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
	   $.post("<?php echo U('Admin/agent/settingEditIsShow');?>",{id:v,status:t},function(data,textStatus){
			var json = WST.toJson(data);
			if(json.status=='1'){
				Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
				    location.reload();
				}});
			}else{
				Plugins.closeWindow();
				Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
			}
	   });
   }

   </script>
   <body class='wst-page'>
        <div class="wst-body">
        <table class="table table-hover table-striped table-bordered wst-list">
           <thead>
             <tr>
               <th width='40'>序号</th>
                 <th>返利级数</th>
                 <th >返利比例</th>
                 <th >提现最小金额</th>
                 <th >提现最大金额</th>
                 <th >申请间隔天数</th>

                 <th>提现密码</th>
                 <th style="display: none">自动分佣</th>
                 <th width='50'>状态</th>
                 <th width='80'>分销模块</th>
                 <th width='100'>操作</th>
             </tr>
           </thead>
            <tbody>
            <?php if(is_array($Page['root'])): $i = 0; $__LIST__ = $Page['root'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($i); ?></td>
                    <td><?php echo ($vo['agentLevel']); ?></td>
                    <td><?php echo ($vo['agentProportion']); ?></td>
                    <td><?php echo ($vo['minApplyPrice']); ?></td>
                    <td><?php echo ($vo['maxApplyPrice']); ?></td>
                    <td><?php echo ($vo['applyDay']); ?></td>

                    <td>
                        <?php if($vo['applyPw']==1 ): ?><span class='label label-success'>需要</span>
                            <?php else: ?>
                            <span class='label label-primary'>不需要</span><?php endif; ?>
                    </td>
                    <td style="display: none">
                        <?php if($vo['agentLogStatus']==1 ): ?><span class='label label-success'>开启</span>
                            <?php else: ?>
                            <span class='label label-primary'>关闭</span><?php endif; ?>
                    </td>
                    <td>
                        <?php if($vo['status']==1 ): ?><span class='label label-success'>开启</span>
                            <?php else: ?>
                            <span class='label label-danger'>关闭</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="dropdown">
                            <?php if($vo['status']==0 ): ?><button class="btn btn-danger dropdown-toggle wst-btn-dropdown"  type="button" data-toggle="dropdown">
                                    关闭
                                    <span class="caret"></span>
                                </button>
                                <?php else: ?>
                                <button class="btn btn-success dropdown-toggle wst-btn-dropdown" type="button" data-toggle="dropdown">
                                    开启
                                    <span class="caret"></span>
                                </button><?php endif; ?>
                            <?php if(in_array('sqlb_02',$WST_STAFF['grant'])){ ?>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(1,<?php echo ($vo['status']); ?>)">开启</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(0,<?php echo ($vo['status']); ?>)">关闭</a></li>
                            </ul>
                            <?php } ?>
                        </div>
                    </td>
                    <td>
                        <?php if(in_array('sqlb_02',$WST_STAFF['grant'])){ ?>
                        <a class="btn btn-default glyphicon glyphicon-pencil" href="<?php echo U('Admin/Agent/settingtoEdit',array('id'=>$vo['communityId']));?>"">修改</a>
                        <?php } ?>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        </div>
   </body>
</html>