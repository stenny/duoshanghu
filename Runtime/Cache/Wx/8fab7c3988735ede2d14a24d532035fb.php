<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php if($_GET['r'] == 'car' ): ?>购物车
	<?php elseif($_GET['r'] == 'index' ): ?>
	首页
	<?php elseif($_GET['r'] == 'cate' ): ?>
分类
	<?php elseif($_GET['r'] == 'my' ): ?>
	我的
	<?php elseif($_GET['r'] == 'score' ): ?>
	积分乐园
	<?php else: ?>
	首页<?php endif; ?></title>
<meta name="viewport"
	content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<!--标准mui.css-->
<link rel="stylesheet" href="/Tpl/Wx/css/supermarket.css" />
<link rel="stylesheet" href="/Tpl/Wx/css/mui.min.css">
<link rel="stylesheet" href="/Tpl/Wx/css/font.css" />
<link rel="stylesheet" type="text/css" href="/Tpl/Wx/css/pub.css" />
<link rel="stylesheet" type="text/css" href="/Tpl/Wx/css/app.css" />
<link rel="stylesheet" type="text/css" href="/Tpl/Wx/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="/Tpl/Wx/css/home.css" />
<link rel="stylesheet" type="text/css" href="/Tpl/Wx/css/goshop.css" />
<link rel="stylesheet" href="/Tpl/Wx/css/my.css" />
<link rel="stylesheet" href="/Tpl/Wx/css/supermarket.css" />
<link rel="stylesheet" href="/Tpl/Wx/css/orderDetail.css" />
<script src="/Tpl/Wx/js/jquery2.1.1.min.js"></script>
<!--<script type="text/javascript" src="/Tpl/Wx/js/iscroll.js"></script>-->
<script type="text/javascript" src="/Tpl/Wx/js/q.js"></script>
	<script type="text/javascript" src="/Tpl/Wx/js/vue.js"></script>
<script src="/Tpl/Wx/js/swiper.min.js"></script>
<script src="/Tpl/Wx/js/index.js"></script>
<script src="/Tpl/Wx/js/base.js"></script>
<script src="/Tpl/Wx/js/cartpaylist.js"></script>
<script src="/Tpl/Wx/js/layer.min.js"></script>

<style>
body{font-size:1.4rem;}
.lh20{height:20px;line-height:20px;}
.title {
	margin: 20px 15px 10px;
	color: #6d6d72;
	font-size: 15px;
}
.showDel{display:none; text-align:right;}
.delCartGoods{text-align:right;margin:2px 8px;}
.delSelect{display:none;}
.oa-contact-cell.mui-table .mui-table-cell {
	padding: 11px 0;
	vertical-align: middle;
}

.oa-contact-cell {
	position: relative;
	margin: -11px 0;
}

.oa-contact-avatar {
	width: 75px;
}

.oa-contact-avatar img {
	border-radius: 50%;
}

.oa-contact-content {
	width: 100%;
}

.oa-contact-name {
	margin-right: 20px;
}

.oa-contact-name, oa-contact-position {
	float: left;
}
.mar-t10{margin-top:8px;}
ul, li {
	margin: 0;
	padding: 0;
}

.swiper-slide img {
	height: 180px;
	max-width: 100%;
	/*width: 100%;*/
}

body {
	padding-bottom: 50px;
	font-size: 14px;
}

.hideDiv {
}

._dian {
	background: url(/Tpl/Wx/image/dianjia_logo.png) center no-repeat;
	width: 25px;
	background-size: 20px;
}

.font90 {
	font-size: 90%;
}

#max_lists {
	background: #fff;
}

.text {
	border: #ccc solid 1px;;
	background: transparent;
}

#max_lists {
	border-bottom: none;
}

p, ul, li, h1, h2, input, select, hr {
	background: none;
}

#max_lists li {
	width: 25%;
	overflow: hidden;
	line-height: 80px;
	height: 80px;
}

#max_lists ul li img {
	padding-top: 10px;
}

#lists li {
	height: 55px;
}

.lh35 {
	height: 35px;
	line-height: 35px;
	text-align: center;
}

.price p {
	height: 15px;
	line-height: 15px;
}

._car_jian {
	background: url(/Tpl/Wx/image/jian_.png) center no-repeat;
	background-size: 80% 80%;
}

._car_jia {
	background: url(/Tpl/Wx/image/jia_.png) center no-repeat;
	background-size: 80% 80%;
}

input[placeholder], [placeholder], *[placeholder] {
	color: #ccc;
	font-size: 90%;
}

::-webkit-input-placeholder {
	color: #ccc;
	font-size: 90%;
}

.indexSearch {
	background: url(/Tpl/Wx/image/index_searc_ico.png) 5px no-repeat;
	background-size: 25px;
	color: #fff;
	width: 100%;
	height:30px;
	line-height: 30px;
	border: none;
	border-radius: 15px;
	margin-top:0.5rem;
	color: #fff;
	background-color: rgba(0, 0, 0, 0.3);
}
.indexSearch input{border:none;}
.location_ico {
	width: 6rem;
	line-height: 4rem;
	background: url(/Tpl/Wx/image/location_ico.png) 5px no-repeat;
	background-size: 15px;
	text-indent: 25px;
	color: #fff
}

.searchNav {
	position: fixed;
	top: 45px;
	left: 0;
	z-index: 9999;
	height: 44px;
	line-height: 44px;
}

._imgWidth img {
	height: 93px;
	overflow: hidden;
}

._img_jin {
	height: 130px;
}

.cash {
	display: block;
	padding: 10px;
	border-radius: 4px;
	margin: 0 auto;
	text-align: center;
	border: #00baff solid 1px;
	color: #00baff;
	width: 80%;
}

.noborder{
	border: none;
}

.imgH50 img{
	height: 50px;
}

.nomargin {
	margin: 0;
}

._imgHeight img {
	height: 145px;
}

._new_img_jin {
	height: auto;
	margin: 0;
	background: #fff;
	margin-right: 3px;
	padding: 8px;
}

.margin_top2 {
	margin-top: 2px;
}
._jin_money{padding-bottom:5px;}
.margin-top8 {
	margin-top: 8px;
}

.nobg, .nopadding {
	background: none;
	padding: 0
}

.duanMoney {
	display: block;
	padding: 5px 10px;
	text-align: center;
}

.dhuan {
	display: block;
	color: #fff;
	background: #00baff;
	border-radius: 4px;
	padding: 5px 10px;
	text-align: center;
}
#editCart{display:none; text-align:center; color:#fff;}
.lh45{height:45px; line-height:45px;}
._contain{width:100%; height:183px;background:#fff;}
._contain a{display:block;float:left;width:33%;}
._imgs{width:100%;overflow:hidden; text-align:center;height:96px;padding:8px;}
._imgs img{height:100%;}
.goodstitle{padding:0px 8px;width:90%; overflow:hidden; white-space:nowrap;text-overflow:ellipsis;height:20px;text-align: center;}
.col-red{color:#ff4045;}
._titles{text-align:center; color:#fff; font-size:16px;};
.layui-layer-btn a{font-weight:normal;}
#max_lists li img{height:40px;}
#max_lists li a{height:20px; line-height:20px;}
#banner ul li a{font-size:1.4rem;}
.mui-bar-tab .mui-tab-item .mui-icon~.mui-tab-label{font-size:1.4rem;}
#max_lists li a{font-size:1.4rem;}
#lists li a{font-size:1.3rem;}
.bor_b{border-bottom:#d5d7dc solid 1px;}
.swiper-container{min-height:180px;}
.gotoSerach{width:30px;}
.car_input{
	width:30px;
    background: transparent;
    border: none;
    height: 30px;
    outline: none;
    line-height: 30px;
    padding: 0;
    margin: 0;
    text-indent: 20px;
    background: url(/Tpl/Wx/image/blace_search.png )  center no-repeat;
    background-size: 20px;
}
#tou{background:#ff4045}
#lists li span{background-color:#ff4045}


.Hotswiper-container {
	width: 100%;
	height: 50px;
	margin-left: auto;
	margin-right: auto;
	overflow: hidden;
}

.dibu {
	height: 50px;
	width: 100%;
	position: relative;
	text-align: center;
}
.di_xian {
	position: absolute;
	margin: 0 5%;
	z-index: 0;
	background-color: #666;
	height: 1px;
	width: 90%;
	top: 24px;
}
.di_title {
	height: 50px;
	line-height: 50px;
	text-align: center;
	padding: 0 10px;
	background-color: #f2f2f2;
	position: relative;
	z-index: 1;
	font-size: 12px;
}
.hideDiv{display:none}
<?php if($_GET['r'] == 'car' ): ?>#page4{display:bolck;}
	<?php elseif($_GET['r'] == 'index' ): ?>
	#page1{display:block}
	<?php elseif($_GET['r'] == 'cate' ): ?>
	#page2{display:block}
	<?php elseif($_GET['r'] == 'my' ): ?>
	#page5{display:block}
	<?php elseif($_GET['r'] == 'score' ): ?>
	#page3{display:block}
	<?php else: ?>
	#page1{display:block}<?php endif; ?>
.car_search{position:fixed;z-index:9999;}
</style>
</head>
<body id="app">
	<div class="ub" style="height:45px; line-height:45px; position:fixed;top:0;left:0;width:100%;background: #ff4045; z-index:999;">
		<div style="width:4rem;"></div>
		<div class="ub-f1 _titles">
		<?php if($_GET['r'] == 'car' ): ?>购物车
	<?php elseif($_GET['r'] == 'index' ): ?>
	首页
	<?php elseif($_GET['r'] == 'cate' ): ?>
分类
	<?php elseif($_GET['r'] == 'my' ): ?>
	我的
	<?php elseif($_GET['r'] == 'score' ): ?>
	积分乐园
	<?php else: ?>
	首页<?php endif; ?>
		</div>
		<div style="width:4rem;"><span id="editCart" data-status="0"  style="<?php if($_GET['r'] == 'car' ): ?>display:block;<?php else: ?>display:none;<?php endif; ?>">编辑</span></div>
	</div>


	<div class="ub searchNav" <?php if($_GET['r'] == 'index' or empty($_GET['r']) ): else: ?>style="display:none;"<?php endif; ?>	>
		<div style="width: 5rem;"></div>
		<div class="ub-f1 ">
			<form id="indexForm" action="<?php echo U('Search/search',array('type'=>'goods'));?>" method="get">
				<div class="ub indexSearch">
				<div class="gotoSerach"></div>
					<div class="ub-f1">
						<input class="" type="input" name="key" value=""
						placeholder="输入商品名称/店铺名称" style="" />
					</div>
				</div>
			</form>
		</div>
		<div class="location_ico"><?php echo ($cityInfo['city']); ?></div>
	</div>


	<nav class="mui-bar mui-bar-tab"
		style="border-top: #d5d7dc solid 1px; background: #fff;">
		<a class="mui-tab-item" href="#tabbar" onclick="page(1)"> <span
			class="mui-icon"><img id="one" style="width: 20px;"
				src="<?php if($_GET['r'] == 'index' or empty($_GET['r']) ): ?>/Tpl/Wx/image/home_click_2.png<?php else: ?>/Tpl/Wx/image/home_click_1.png<?php endif; ?>"></span> <span class="mui-tab-label">首页</span>
		</a> <a class="mui-tab-item" href="#tabbar-with-chat" onclick="page(2)">
			<span class="mui-icon "><img id="two" style="width: 20px;"
				src="<?php if($_GET['r'] == 'cate' ): ?>/Tpl/Wx/image/fenlei_click_2.png<?php else: ?>/Tpl/Wx/image/fenlei_click_1.png<?php endif; ?>"></span> <span class="mui-tab-label">分类</span>
		</a> <a class="mui-tab-item" href="#tabbar-with-contact" onclick="page(3)">
			<span class="mui-icon "><img id="three" style="width: 20px;"
				src="<?php if($_GET['r'] == 'score' ): ?>/Tpl/Wx/image/jifen_click_2.png<?php else: ?>/Tpl/Wx/image/jifen_click_1.png<?php endif; ?>"></span> <span class="mui-tab-label">积分乐园</span>
		</a> <a class="mui-tab-item" href="#tabbar-with-map" onclick="page(4)">
			<span class="mui-icon"><img id="four" style="width: 20px;"
				src="<?php if($_GET['r'] == 'car' ): ?>/Tpl/Wx/image/goshop_click_2.png<?php else: ?>/Tpl/Wx/image/goshop_click_1.png<?php endif; ?>"></span> <span class="mui-badge"
			style="position: absolute; top: 0;"><?php echo ($goodsCnt); ?></span> <span
			class="mui-tab-label">购物车</span>
		</a> <a class="mui-tab-item" href="#tabbar-with-my" onclick="page(5)">
			<span class="mui-icon "><img id="five" style="width: 20px;"
				src="<?php if($_GET['r'] == 'my' ): ?>/Tpl/Wx/image/my_click_2.png<?php else: ?>/Tpl/Wx/image/my_click_1.png<?php endif; ?>"></span> <span class="mui-tab-label">我的</span>
		</a>
	</nav>
	<div class="" style="margin-top: 44px;">
		<div id="page1"  class="hideDiv">
			<!--第一个TAB-->
				<div class="swiper-container one_swiper visible-xs-block">
					<div class="swiper-wrapper" id="imgList">
					<?php  ?>
						<?php if(is_array($adInfo)): $i = 0; $__LIST__ = $adInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ad): $mod = ($i % 2 );++$i;?><div onclick="goto(this)" data-url="<?php echo ($ads["adURL"]); ?>" data-da="<?php echo ($ads["adURLApp"]); ?>" class="swiper-slide">
							<a href="<?php echo ($ad["adurl"]); ?>"><img src="/<?php echo ($ad["adfile"]); ?>" /></a>
						</div><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
					<div class="swiper-pagination one-pagination"></div>
				</div>
			<div id="banner">
				<ul>
					<li  class="ban1" onclick="javascript:window.location.href='<?php echo U('supermarket',array('parentId'=>428));?>'"><img
						src="/Tpl/Wx/image/fzc.png" /><a>服装城</a></li>
					<li onclick="javascript:window.location.href='<?php echo U('supermarket',array('parentId'=>429));?>'"
						class="ban2"><img src="/Tpl/Wx/image/chaoshi.png" /><a>超市</a></li>
					<li onclick="javascript:window.location.href='<?php echo U('MustBuy/index',array('parentId'=>430));?>'" class="ban3"><img
						src="/Tpl/Wx/image/bimai.png" /><a>必买专区</a></li>
					<li onclick="javascript:window.location.href='<?php echo U('Group/search');?>'"
						class="ban4"><img src="/Tpl/Wx/image/tuangou.png" /><a>团购</a></li>
				</ul>
			</div>

			<div class="_son_title  _margin_top10" style="margin-top:10px;">
			<input type="hidden" id="secKill_page" value="1" />
				<div class="_son_title_left"><b>天天秒杀</b></div>
				<div class="_son_title_right"  style="background:none;text-align:right;padding:0;color:#ff4045;"><a href="<?php echo U('SecKill/index');?>">全场9.9元起</a></div>
				<div class="right_arrows" style="background-size:8px;"></div>
				<!-- onclick="javascript:secKill(this);"
					data-url="<?php echo U('secKill');?>"  -->
			</div>
			<div class="_contain bor_b" style="overflow: hidden" id="secKill_html">
				<div class="swiper-container_secKill">
					<div class="swiper-wrapper">
						<?php if(is_array($secKillInfo)): $i = 0; $__LIST__ = $secKillInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sk): $mod = ($i % 2 );++$i;?><div style="width: 33%" class="swiper-slide">
								<a style="width: 100%"  href="<?php echo U('Index/goodsDetail',array('id'=>$sk['goodsId'],'secKill'=>1));?>">
									<div class="_imgs"><img src="/<?php echo ($sk["goodsThums"]); ?>" /></div>
									<div class="txt-l goodstitle"><?php echo ($sk["goodsName"]); ?></div>
									<div class="txt-l goodstitle col-red">￥<?php echo ($sk["seckillPrice"]); ?></div>
									<div class="txt-l goodstitle"><del>￥<?php echo ($sk["shopPrice"]); ?></del></div>
								</a>
								</div><?php endforeach; endif; else: echo "" ;endif; ?>


					</div>
				</div>

			</div>
			<!--
			<a class="Hotswiper-slide" onclick="alert('123');" href="">
				<span class="tt1"></span>
				<span class="tt2">商城头条:</span>
				<span class="tt3"><img src="/<?php echo ($ads["adFile"]); ?>" /><?php echo ($ads["adName"]); ?></span>
			</a>
			-->
			<div class="Hotswiper-container tu-title">

					<div class="swiper-wrapper">
						<?php if(is_array($indexHotAd)): $i = 0; $__LIST__ = $indexHotAd;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ads): $mod = ($i % 2 );++$i;?><div class="swiper-slide">
							<a onclick="goto(this)" data-da="<?php echo ($ads["adURLApp"]); ?>">
								<span class="tt1"></span>
								<span class="tt2">商城头条:</span>
								<span style="color: red;font-weight: bold">HOT</span>
								<span style="overflow: hidden;white-space:normal;height: 50px;text-overflow:ellipsis;width:200px;-o-text-overflow:ellipsis;"><?php echo ($ads["adName"]); ?></span>
							</a>
						</div><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>

			</div>


			<div id="tu-list">
				<div tapmode="a" onclick="javascript:window.location.href='/Go/Index/index'"  class="t-l-1" style="background-image:url('/Tpl/Wx/image/tu_1.png');background-size:100% 100%;">
				</div>
				<div class="t-l-2"></div>
				<div class="t-l-3">
					<div tapmode="a" onclick="javascript:window.location.href='/Wx/MustBuy/index/parentId/430.html'" class="t-l-31" style="background-image:url('/Tpl/Wx/image/tu_2.png');background-size:100% 100%;"></div>
					<div class="t-l-32"></div>
					<div tapmode="a" onclick="javascript:window.location.href='<?php echo U('Group/search');?>'" class="t-l-33" style="background-image:url('/Tpl/Wx/image/tu_3.png');background-size:100% 100%;"></div>
				</div>
			</div>

			<a data-da="<?php echo ($bigAd["adURLApp"]); ?>" onclick="goto(this)" data-url="<?php echo ($$bigAd["adURL"]); ?>">
			<img src="/<?php echo ($bigAd["adFile"]); ?>" style="width: 100%;height: 100%;min-height: 50px" />
			</a>
			<!--<div class="_contain">-->
				<!---->


				<!--<div  style="background-image:url('/Tpl/Wx/image/tu_4.png');background-size:100% 100%;">-->
				<!--</div>-->
			<!--</div>-->

			<!--<div  style="background-image:url('/Tpl/Wx/image/tu_4.png');background-size:100% 100%;">-->







			<div class="_son_title  _margin_top10" >
				<div class="_son_title_left"><b>精品选购</b></div>
				<input type="hidden" id="isSale_page" value="1" />
				<div class="_son_title_right" onclick="javascript:isSale(this);"
					data-url="<?php echo U('boutique');?>" data-type="isBest"
					data-input="isSale_page" data-id="isSale">换一组</div>
			</div>
			<!-- -----精品------ -->
			<div class="_contain bor_b" id="isSale" >
				<?php if(is_array($isBest)): $i = 0; $__LIST__ = $isBest;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sk): $mod = ($i % 2 );++$i;?><a href="<?php echo U('Index/goodsDetail',array('id'=>$sk['goodsId'],'r'=>'index'));?>">
				<div class="_imgs"><img src="/<?php echo ($sk["goodsThums"]); ?>" /></div>
					<div class="txt-l goodstitle"><?php echo ($sk["goodsName"]); ?></div>
					<div class="txt-l goodstitle col-red">￥<?php echo ($sk["shopPrice"]); ?></div>
                </a><?php endforeach; endif; else: echo "" ;endif; ?>
			</div>

			<!-- -----精品------ -->


			<!-- 新品上市 -->
			<div class="_son_title  _margin_top10">
				<div class="_son_title_left"><b>新品上市</b></div>
				<input type="hidden" id="isNew_page" value="1" />
				<div class="_son_title_right" onclick="javascript:isSale(this);"
					data-url="<?php echo U('boutique');?>" data-type="isNew"
					data-input="isNew_page" data-id="isNew">换一组</div>
			</div>
			<div class="_contain bor_b" id="isNew">
				<?php if(is_array($isNew)): $i = 0; $__LIST__ = $isNew;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sk): $mod = ($i % 2 );++$i;?><a href="<?php echo U('Index/goodsDetail',array('id'=>$sk['goodsId'],'r'=>'index'));?>">
				<div class="_imgs"><img src="/<?php echo ($sk["goodsThums"]); ?>" /></div>
					<div class="txt-l goodstitle"><?php echo ($sk["goodsName"]); ?></div>
					<div class="txt-l goodstitle col-red">￥<?php echo ($sk["shopPrice"]); ?></div>
				</a><?php endforeach; endif; else: echo "" ;endif; ?>
			</div>

			<!-- 热销商品 -->
			<div class="_son_title  _margin_top10">
				<div class="_son_title_left bold"><b>热销商品</b></div>
				<input type="hidden" id="isHot_page" value="1" />
				<div class="_son_title_right" onclick="javascript:isSale(this);"
					data-url="<?php echo U('boutique');?>" data-type="isHot"
					data-input="isHot_page" data-id="isHot">换一组</div>
			</div>
			<div class="_contain bor_b" id="isHot">
				<?php if(is_array($isHot)): $i = 0; $__LIST__ = $isHot;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sk): $mod = ($i % 2 );++$i;?><a href="<?php echo U('Index/goodsDetail',array('id'=>$sk['goodsId'],'r'=>'index'));?>">
				<div class="_imgs"><img src="/<?php echo ($sk["goodsThums"]); ?>" /></div>
					<div class="txt-l goodstitle"><?php echo ($sk["goodsName"]); ?></div>
					<div class="txt-l goodstitle col-red">￥<?php echo ($sk["shopPrice"]); ?></div>
				</a><?php endforeach; endif; else: echo "" ;endif; ?>
			</div>
			<!--第一个TAB结束-->

		</div>
		<!--------------------------- 分类 ----------------------->
		<div id="page2" class="hideDiv">
			<div class=" back-ff car_search">
				<div class="car_se">
				<form id="cateForm" action="<?php echo U('Search/search',array('type'=>'goods'));?>" method="get">
					<div class="ub ">
						<div class="car_input"></div>
						<div class="ub-f1">
						<input type="text" class="" name="key" style="width:100%; border:none;"
						id="searchKey" value="" placeholder="输入你喜欢的商店、商品" />
						</div>
					</div>

						</form>
				</div>
			</div>

			<template id="child-template">

				<?php if(is_array($cate)): $i = 0; $__LIST__ = $cate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ca): $mod = ($i % 2 );++$i;?><li v-on:click="addTodo()"  data-url="/<?php echo ($ca['info']['adImg']); ?>"  data-id="<?php echo ($ca['info']['catId']); ?>"><a href="#"><?php echo ($ca['info']['catName']); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			</template>

			<div class="mg-auto overflow">
					<!--菜单-->
					<div class="Menu_box">
						<!---------左侧菜单---------->
						<div id="left_Menu">
							<div class="ot-menu" id="scroller">
								<ul>
									<child></child>
									<!--<li class="active"><a href="#"><b>热门推荐</b></a></li>-->
										<!--<?php if(is_array($cate)): $i = 0; $__LIST__ = $cate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ca): $mod = ($i % 2 );++$i;?>-->

											<!--<li v-on:click="addTodo(<?php echo ($ca['info']['catId']); ?>,this)"  data-id="<?php echo ($ca['info']['catId']); ?>"><a href="#"><?php echo ($ca['info']['catName']); ?></a></li>-->

										<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->






								<!--<li onclick="javascript::void()" style="height:70px; border:none;"><a href="#">&nbsp;</a></li>-->
								</ul>
							</div>
						</div>

						<!---------右侧侧菜单---------->
						<div id="right_Menu">
							<div class="ot-maininfo">
								<div class="menutype">
									<!-- --------右侧广告----------- -->
									<div id="cart_right_ad">
										<a href="#"><img :src="topImg"></a>
									</div>

									<div class="cart_son_li" v-for="(key, val) in rightItems">
										<div class="cart_son_li_title">{{val.info.catName}}</div>
										<ul>
											<li v-for="(xkey,xval) in val.child">
												<a href="/Wx/Cate/cate/id/{{xval.info.catId}}">
												<img :src="'/'+xval.info.cateImg" />
												<p>{{xval.info.catName}}</p>
												</a>
											</li>
										</ul>
									</div>


								</div>
								<!-- --------右侧广告----------- -->
<!--
								<?php if(is_array($cate)): $i = 0; $__LIST__ = $cate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ca): $mod = ($i % 2 );++$i;?><div class="menutype">

									<div id="cart_right_ad">
										<a href="#">
											<?php if($ca['info']['adImg']): ?><img  style="height: 100%" src="/<?php echo ($ca['info']['adImg']); ?>">
												<?php else: ?>
												<img src="/Tpl/Wx/image/cart_ad.png"><?php endif; ?>
										</a>
									</div>
									<?php if(is_array($ca['child'])): $i = 0; $__LIST__ = $ca['child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$son): $mod = ($i % 2 );++$i;?><div class="cart_son_li">
										<div class="cart_son_li_title"><?php echo ($son['info']["catName"]); ?></div>
										<ul>
											<?php if(is_array($son['child'])): $i = 0; $__LIST__ = $son['child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$Grandson): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Cate/cate',array('id'=>$key));?>"><img src="/<?php echo ($Grandson['info']["cateImg"]); ?>" />
												<p><?php echo ($Grandson['info']["catName"]); ?></p></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
										</ul>
									</div><?php endforeach; endif; else: echo "" ;endif; ?>
								</div><?php endforeach; endif; else: echo "" ;endif; ?>
								-->
							</div>
						</div>
					</div>
					<!--菜单end-->
				</div>

		</div>
		<!-- ---------------------分类结束-------------------------- -->
		<!------------------------------- 职分乐园 ------------------->
		<div id="page3" class="hideDiv">
				<div class="swiper-container four_swiper visible-xs-block">
					<div class="swiper-wrapper" id="imgList">
						<?php if(is_array($adInfo)): $i = 0; $__LIST__ = $adInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ad): $mod = ($i % 2 );++$i;?><div onclick="goto(this)" data-da="<?php echo ($ads["adURLApp"]); ?>" data-url="<?php echo ($ads["adURL"]); ?>" class="swiper-slide">
								<img src="/<?php echo ($ad["adfile"]); ?>" />
						</div><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
					<div class="swiper-pagination four-pagination"></div>
				</div>
			<!-- -----轮播结束------- -->
			<div class="ub back-ff padding_bottom10 padding_top10">
				<div class="ub-f2">
					<p class="col_6 text-l lh20 font110">
						我的积分:<font class="col_red"><?php echo ($userScore); ?></font>
					</p>
					<p class="col_9 text-l lh20"><a href="<?php echo U('Score/explain');?>">如何获得更多积分?</a></p>
				</div>
				<div class="ub-f1">
					<a href="<?php echo U('Score/record',array('r'=>'score','ref'=>'Index_index'));?>">
					<span class="cash">兑换记录</span>
					</a>
				</div>
			</div>
			<!-- --兑换记录- -->
			<div id="max_lists" class="imgH50 noborder" style="height: 100px;">
				<ul>
					<li onclick="location.href='<?php echo U('Score/scoreList',array('catid'=>1));?>'" class="noborder " style="border:none;"><img style="height:50px;" src="/Tpl/Wx/image/today_new.png" /><a>今日新品</a></li>
					<li onclick="location.href='<?php echo U('Score/scoreList',array('catid'=>2));?>'" class="noborder imgH50 " style="border:none;"><img  style="height:50px;"  src="/Tpl/Wx/image/meizhuan.png" /><a>个护美妆</a></li>
					<li onclick="location.href='<?php echo U('Score/scoreList',array('catid'=>3));?>'" class="noborder" style="border:none;"><img   style="height:50px;"  src="/Tpl/Wx/image/jiaji.png" /><a>日常家居</a></li>
					<li onclick="location.href='<?php echo U('Score/scoreList',array('catid'=>4));?>'" class="noborder" style="border:none;"><img   style="height:50px;" src="/Tpl/Wx/image/jiadian.png" /><a>数码家电</a></li>
				</ul>
			</div>
			<div class="ub height35 back-ff back-ef font90 " style="height: 100%">


				<div class="dibu">
					<div class="di_xian"></div>
					<a class="di_title">兑换商品</a>
				</div>

				<!--<div class="ub-f1 margin_left10 ">-->
					<!--<hr-->
						<!--style="margin-top: 17px; border: 0; background-color: #999; height: 1px;">-->
				<!--</div>-->
				<!--<div class="ub-f1 col_9 pos-r text-c">-->
					<!--<div class="pos-a">兑换商品</div>-->

				<!--</div>-->
				<!--<div class="ub-f1 margin_right10">-->
					<!--<hr-->
						<!--style="margin-top: 17px; border: 0; background-color: #999; height: 1px;">-->
				<!--</div>-->
			</div>
			<style>
._contain ar{background:#fff;}
.lh30{height:30px; line-height:30px;}
			.score_a ar:nth-child(odd){float:left;width:49.5%;margin-bottom:4px;}
			.score_a ar:nth-child(even){float:right;width:49.5%;margin-bottom:4px;}
			</style>
			<div class="_contain score_a" style="height:218px;background:none;">
				<?php if(is_array($scoreGoodsInfo)): $i = 0; $__LIST__ = $scoreGoodsInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sk): $mod = ($i % 2 );++$i;?><ar>
						<div class="_imgs" style='height:145px'>
							<a  style='height:145px;width: 100%' href="<?php echo U('Score/scoreGoods',array('id'=>$sk['goodsId'],'ref'=>'score'));?>">
								<img src="/<?php echo ($sk["goodsThums"]); ?>" />
							</a>
						</div>
					<div class="txt-l goodstitle lh30" ><?php echo ($sk["goodsName"]); ?></div>
					<div class="_jin_money ub">
							<div class="ub-f1 ">
								<span class="duanMoney col-red"><?php echo ($sk["shopPrice"]); ?>积分</span>
							</div>
								<div class="" >
									<a style="margin-right:8px;width:60px" href="<?php echo U('Score/scorepay',array('id'=>$sk['goodsId'],'ref'=>'score'));?>">
									<span class="dhuan">兑换</span>
									</a>
								</div>

						</div>

						</ar><?php endforeach; endif; else: echo "" ;endif; ?>
			</div>
		</div>
		<!-- ------------------职分乐园结束---------------------- -->
		<!-- ----------------------购物车 ----------------------------->
		<div id="page4" class="hideDiv" style="margin-bottom: 50px ; <?php if($_GET['r'] == 'car' ): ?>display:block;<?php endif; ?>">
			<div class="_content">
				<?php if(is_array($cartInfo['cartgoods'])): $i = 0; $__LIST__ = $cartInfo['cartgoods'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$cart): $mod = ($i % 2 );++$i;?><div class="_son_title ub mar-t10"
					onclick="javascript:window.location.href='<?php echo U("Shop/shop",array("id"=>$key));?>'">
					<div class="title_left_ico margin_left10 " style="background:url(/<?php echo ($cart['shopgoods'][0]['shopImg']); ?>) center no-repeat;background-size:26px 26px;"></div>
					<div class="ub-f1 text-l"><?php echo ($cart['shopgoods'][0]['shopName']); ?></div>
					<div class="right_arrows"></div>
				</div>
				<?php if(is_array($cart['shopgoods'])): $i = 0; $__LIST__ = $cart['shopgoods'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?><input data-id="<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  id="stock_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  type="hidden" value="<?php if($list['isGroup'] == 1 || $list['isSeckill'] == 1): if($list['goodsStock'] < $list['cnt']): ?>-1<?php else: ?>1<?php endif; ?> ><?php else: if($list['goodsAttrId'] == 0): if($list['goodsStock'] < $list['cnt']): ?>-1<?php else: ?>1<?php endif; else: ?> <?php if($list[0]['goodsStock'] < $list['cnt']): ?>-2<?php else: ?>1<?php endif; endif; endif; ?>"class="goodsStockFlag"/>
				<?php if($list['isGroup'] == 1): ?><input data-id="<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  id="group_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  type="hidden" value="<?php if($list['groupStatus'] == 1): ?>1<?php else: ?>-1<?php endif; ?>" class="groupFlag"/><?php endif; ?>

				<?php if($list['isSeckill'] == 1): ?><input data-id="<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  id="group_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  type="hidden" value="<?php if($list['seckillStatus'] == 1): ?>1<?php else: ?>-1<?php endif; ?>" class="secKillFlag"/><?php endif; ?>

				<input data-id="<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  id="deliveryStarMoney_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"  type="hidden" value="<?php echo ($list['deliveryStartMoney']); ?>" class="deliveryFlag"/>
				<div class="ub"  id="color_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"
				<?php if($list['isGroup'] == 1 || $list['isSeckill'] == 1): if($list['isGroup'] == 1): if($list['groupStatus'] == 1&&$list['goodsStock'] >= $list['cnt']): ?>style="background: #fff; padding-top: 10px;"
							<?php else: ?>
							style="background: #D5D7DC; padding-top: 10px;"<?php endif; endif; ?>
						<?php if($list['isSeckill'] == 1): if($list['seckillStatus'] == 1&&$list['goodsStock'] >= $list['cnt']): ?>style="background: #fff; padding-top: 10px;"
							<?php else: ?>
							style="background: #D5D7DC; padding-top: 10px;"<?php endif; endif; ?>
				 		 >
				<?php else: ?>
					<?php if($list['goodsAttrId'] == 0): if($list['goodsStock'] < $list['cnt']): ?>style="background: #D5D7DC; padding-top: 10px;"<?php else: ?>
							 style="background: #fff; padding-top: 10px;"<?php endif; ?>
					<?php else: ?>
						<?php if($list[0]['goodsStock'] < $list['cnt'] || $list[0]['goodsStock'] < 1): ?>style="background: #D5D7DC; padding-top: 10px;"
						 <?php else: ?>
							 style="background: #fff; padding-top: 10px;"<?php endif; endif; ?>
					 ><?php endif; ?>
					<div class="margin_left10 margin_right10" style="width:80px; overflow:hidden;">
						<a href="<?php echo U('Index/goodsDetail',array('id'=>$list['goodsId']));?>"><img style="height: 80px;width:80px;" src="/<?php echo ($list["goodsThums"]); ?>" alt=""></a>
					</div>
					<div class="ub-f1  ub  b-v">
						<div class="ub ">
							<div class="ub-f1 pos-r " style="height: 40px; overflow: hidden; ">
								<div class="pos-a  " style="text-align: left;line-height:20px; height:40px; overflow:hidden;"><?php echo ($list["goodsName"]); ?>
								<?php if($list['isSeckill']){ echo '(秒)'; }else if($list['isGroup']){ echo '(团)'; } ?>
								</div>
							</div>
							<div class="">
								<div class="">
									<div
										class="mui-input-row mui-checkbox  margin_right10 margin_top5">
										<input type="checkbox" class="chkall"
											dataid="<?php echo ($list['goodsAttrId']); ?>" data-gid="<?php echo ($list['goodsId']); ?>"
											data-isground="<?php echo ($list['isGroup']); ?>"
											data-isSeckill="<?php echo ($list['isSeckill']); ?>"
											id="chk_goods_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"
											name="chk_goods_<?php echo ($list['shopId']); ?>"
											value="<?php echo ($list['goodsId']); ?>_<?php echo ($goods['goodsAttrId']); ?>"
											parent="<?php echo ($list['shopId']); ?>" isBook="<?php echo ($list['isBook']); ?>"
										<?php if($list['ischk'] == 1): ?>checked<?php endif; ?>
										/> <input type="hidden" class="cgoodsId"
											value="<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="ub-f1 ub pos-r">
							<div class="ub-f1 pos-r">
								<div class="pos-a price">
									<p class="col_red lh20" style="height:20px; line-height:20px;" >
									<?php $shopsPrice=0; if($list['isGroup']||$list['isSeckill']){ $shopsPrice=$list['shopPrice']; }else{ foreach($list as $k=>$v){ if(is_numeric($k)){ $shopsPrice+=$v['shopPrice']; } } if($shopsPrice==0){ $shopsPrice=$list['shopPrice']; } } ?>
										￥
										<?php $totalMoney=0; if($shopsPrice==0){ $totalMoney=$list['shopPrice']*$list['cnt']; echo $list['shopPrice']; }else{ $totalMoney=$shopsPrice*$list['cnt']; echo $shopsPrice; } ?>
									</p>
									<input type="hidden"
										id="price_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"
										value="	<?php echo $shopsPrice; ?>"/>
									<p class="lh20" style="height:20px; line-height:20px;" >
										<span class="col_9">小计:</span>￥ <span
											id="prc_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"> <?php echo $totalMoney; ?>
										</span>
									</p>
								</div>
							</div>
							<div class="ub-f1 pos-r">
								<div class="pos-a">
									<div class="ub lh35 editCart">
										<div class="ub-f1 txt-c showDel"><span class="delCartGoods" data-id="<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>">删除</span></div>
										<div class="ub-f1 pos-r">
											<div class="pos-a _car_jian"
												id="numl_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>"
												onclick="changeCatGoodsnum(1,<?php echo ($list['shopId']); ?>,<?php echo ($list['goodsId']); ?>,'<?php echo ($list['goodsAttrId']); ?>',<?php echo ($list['isBook']); ?>,<?php echo ($list['isSeckill']); ?>,<?php echo ($list['isGroup']); ?>)"></div>
										</div>
										<div class="ub-f1 pos-r ">
											<div class="pos-a font110">
												<input
													id="buy-num_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>_<?php echo ($list['isSeckill']); ?>_<?php echo ($list['isGroup']); ?>"
													dataid="<?php echo ($list['goodsAttrId']); ?>"
													data-isground="<?php echo ($list['isGroup']); ?>" class="text "
													style="width: 30px; text-align: center;" maxlength="3"
													value="<?php echo ($list['cnt']); ?>"
													onkeypress="return WST.isNumberKey(event);"
													onkeyup="changeCatGoodsnum(0,<?php echo ($list['shopId']); ?>,<?php echo ($list['goodsId']); ?>,'<?php echo ($list['goodsAttrId']); ?>',<?php echo ($list['isBook']); ?>,<?php echo ($list['isSeckill']); ?>,<?php echo ($list['isGroup']); ?>);" />
											</div>
										</div>
										<div class="ub-f1 pos-r">
											<div class="pos-a _car_jia"
												id="numr_<?php echo ($list['goodsId']); ?>_<?php echo ($list['goodsAttrId']); ?>"
												onclick="changeCatGoodsnum(2,<?php echo ($list['shopId']); ?>,<?php echo ($list['goodsId']); ?>,'<?php echo ($list['goodsAttrId']); ?>',<?php echo ($list['isBook']); ?>,<?php echo ($list['isSeckill']); ?>,<?php echo ($list['isGroup']); ?>)"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><?php endforeach; endif; else: echo "" ;endif; endforeach; endif; else: echo "" ;endif; ?>
			<!-- 底部总金额 -->
			<div class="ub"
				style="height: 45px; line-height: 45px; color: #fff; position: fixed; bottom: 50px; left: 0;">
				<div class="ub-f2 pos-r" style="background: #a7a7aa;">
					<div clas="pos-a ">
						<div class="ub ">
							<div class=" ">
								<div class="ub">
									<div style="padding-right: 1em"
										class="text-l mui-input-row mui-checkbox  margin_right10 margin_top5 white-checkbox">
										<input id="chk_all" name="checkbox" value="2" type="checkbox" checked>
									</div>
									<div class="ub-f1">全选</div>
								</div>
							</div>
							<div class="ub-f1 text-l">
							<div class="total_select">
								合计：￥ <span id="wst_cart_totalmoney" class="wst-cart-totalmoney"><?php echo ($cartInfo['totalMoney']); ?></span></div>
								<!-- <span id="shop_totalMoney_<?php echo ($list['shopId']); ?>"><?php echo ($cartInfo["totalMoney"]); ?></span> -->
							</div>
						</div>
					</div>
				</div>
				<div class="ub-f1 pos-r" style="background: #ff961b;">
					<div clas="pos-a ">
						<div class="ub">
							<div class="ub-f1 delSelect text-c">删除所选</div>
							<div class="ub-f1 text-c toPay" onclick="goToPay()">
								去结算(<span id="productNum"><?php echo ($goodsCnt); ?></span>)
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 底部总金额 -->
		</div>
		<!-- ----------------------购物车结束 ----------------------------->
	</div>
	<div id="page5" class="hideDiv">
		<!--页面5-->
		<div id="tou">
			<div id='tou-list-1'>
				<div id="tou-list-11"></div>
				<div id="tou-list-12">
					<div id="yq-w">
						<div id="yq-z"  style="<?php if($uinfo['userPhoto']): ?>background-image:url(/<?php echo ($uinfo['userPhoto']); ?>); background-size:100%;background-position:center;<?php endif; ?>"  onclick="location.href='<?php echo U('Login/userInfo');?>'">
							<div id="yq-n"></div>
						</div>
					</div>
				</div>
			</div>
			<div id='tou-list-2'><?php if($uinfo['userName']): echo ($uinfo["userName"]); else: echo ($uinfo["userPhone"]); endif; ?></div>
			<div id="shezhi" onclick="location.href='<?php echo U('Login/userInfo',array('r'=>'my'));?>'"></div>
		</div>
		<div class="ub lh45 back-ff border_bottom1 " onclick="location.href='<?php echo U('Orders/orders',array('r'=>'my'));?>'">
            <div class="col_9 ub-f1 text-l">
              我的订单
            </div>
            <div  class="col_9  font90 text-l ">
             查看全部订单
            </div>
            <div class="right_arrows">
            </div>
        </div>
		<div style="height:10px; width:100%;background:#fff;"></div>
		<div id="lists">
			<ul>
				<li  onclick="location.href='<?php echo U('Orders/waitPay',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/daifukuan.png" /><a>待付款</a><span><?php echo ($stats["waitPay"]); ?></span></li>
				<li  onclick="location.href='<?php echo U('Orders/waitDeliver',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/daifahuo.png" /><a>待发货</a><span><?php echo ($stats["waitDeliver"]); ?></span></li>
				<li  onclick="location.href='<?php echo U('Orders/waitReceiving',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/daishouhuo.png" /><a>待收货</a><span><?php echo ($stats["waitReceiving"]); ?></span></li>
				<li onclick="location.href='<?php echo U('Comment/evaluate',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/daipingjia.png" /><a>待评价</a><span><?php echo ($stats["waitEvaluate"]); ?></span></li>
				<li  onclick="location.href='<?php echo U('Orders/refundList',array('r'=>'my'));?>'" style="border: 0px;"><img src="/Tpl/Wx/image/tuikuan.png" /><a>退款/售后</a><span><?php echo ($stats["waitRefund"]); ?></span></li>
			</ul>
		</div>
		<div id="max_lists">
			<ul>
				<li onclick="location.href='<?php echo U('Ucenter/wallet',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/wodeqianbao.png" /><a>我的钱包</a></li>
				<li onclick="javascript:window.location.href='/Wx/Agent/Index'"><img src="/Tpl/Wx/image/fenxiaozhognxin.png" /><a>分销中心</a></li>
				<li onclick="location.href='<?php echo U('Ucenter/score',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/wodejifen.png" /><a>我的积分</a></li>
				<li onclick="location.href='<?php echo U('Join/protocol');?>'"><img src="/Tpl/Wx/image/woshishagnjia.png" /><a>我是商家</a></li>
			</ul>
			<ul>
				<li><img src="/Tpl/Wx/image/0yuangou.png" /><a>0元购</a></li>
				<li onclick="javascript:window.location.href='<?php echo U('Favorites/index');?>'"><img src="/Tpl/Wx/image/shoucangjia.png" /><a>收藏夹</a></li>
				<li onclick="location.href='<?php echo U('Coupon/coupon',array('r'=>'my'));?>'" ><img src="/Tpl/Wx/image/coupon.png" /><a>优惠卷</a></li>
				<li onclick="location.href='<?php echo U('Service/index');?>'"><img src="/Tpl/Wx/image/bangzhufankui.png" /><a>帮助与反馈</a></li>
			</ul>
			<ul>
				<li onclick="location.href='<?php echo U('Go/Index/index');?>'"><img src="/Tpl/Wx/image/1yuangou.png" /><a>一元购</a></li>
				<li onclick="location.href='<?php echo U('Group/search',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/tuangoujie.png" /><a>团购节</a></li>
				<li onclick="location.href='<?php echo U('SecKill/index',array('r'=>'my'));?>'"><img src="/Tpl/Wx/image/miaoshahuodong.png" /><a>秒杀活动</a></li>
		  		<li><img src="/Tpl/Wx/image/bimaizhuanqu.png" /><a>必买专区</a></li>
			</ul>
		</div>
		<div class="_son_title  _margin_top10">
			<div class="_margin_left10 _dian"
				style="background-image: url(/Tpl/Wx/image/xin.png)"></div>
			<div class="_son_title_left font90"><b>猜你喜欢</b></div>
			<div class="_son_title_right font90" onclick="javascript:guess(this);"data-url="<?php echo U('guess');?>" >换一组</div>
		</div>
			<div class="_contain bor_b" id="guess_html">
				<?php if(is_array($guessInfo)): $i = 0; $__LIST__ = $guessInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sk): $mod = ($i % 2 );++$i;?><a href="<?php echo U('Index/goodsDetail',array('id'=>$sk['goodsId'],'secKill'=>1,'r'=>'my'));?>">
				<div class="_imgs"><img src="/<?php echo ($sk["goodsThums"]); ?>" /></div>
					<div class="txt-l goodstitle"><?php echo ($sk["goodsName"]); ?></div>
					<div class="txt-l goodstitle col-red">￥<?php echo ($sk["shopPrice"]); ?></div>
				</a><?php endforeach; endif; else: echo "" ;endif; ?>
			</div>
		<!--页面5结束-->
	</div>
</body>

<script>
// $('#right_Menu').click(function(){
// 	var top=$('#right_Menu').css('top');
// 	top=top.split("px");
// 	alert(top[0]);
// });
// window.addEventListener('touchstart', function(event) {
//      // 如果这个元素的位置内只有一个手指的话
//     if (event.targetTouches.length == 1) {
// 　　　　	//event.preventDefault();// 阻止浏览器默认事件，重要 
//         var touch = event.targetTouches[0];
//         // 把元素放在手指所在的位置
//         	stat_X=touch.pageX;
//         	stat_Y=touch.pageY;
//         }
// }, false);
// window.addEventListener('touchmove', function(event) {
//      // 如果这个元素的位置内只有一个手指的话
//     if (event.targetTouches.length == 1) {
// 　　　　	//event.preventDefault();// 阻止浏览器默认事件，重要 
//         var touch = event.targetTouches[0];
//         var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
//         var top=$('#right_Menu').css('top');
//         top=top.split("px");
//         //alert(Number(top[0])+(stat_Y-touch.pageY));
        
//         $('#right_Menu').css('top',(Number(top[0])-((stat_Y-touch.pageY)/20))+'px');
//         	//$('body').scrollTop(scrollTop+(stat_Y-touch.pageY));

//         }
// }, false);
// window.addEventListener('touchend', function(event) {
// 	//event.preventDefault();// 阻止浏览器默认事件，重要 
// 	var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
// 	//$('#right_Menu').css('top','87px');
// 	$('#right_Menu').animate({top:'87px'},'slow');
// }, false);
//alert("<?php echo $uinfo['userPhone'];?>");
var lin_userPhone="<?php echo $uinfo['userPhone'];?>";
if(lin_userPhone=="")
{
	var lin_login=localStorage.login;
	var lin_userId=localStorage.userId;
	if(lin_login==1)
	{
		$.ajax({
				type : "POST",
				url : "<?php echo U('Login/indexAutoLogin');?>",
				data : {userId:lin_userId},
				dataType : "json",
				success : function(data) {
					if(data.status==0){
						//_message('成功退出',2);
						location.href="<?php echo U('Index/index');?>";
					}
				}
		});
	}
}
$(function(){
	scrollOne();
	scrollTwo();

	var r="<?php echo ($_GET['r']); ?>";
	if(r=='car'){
		page(4);
	}else if(r=='index'){
		page(1);
	}else if(r=='cate'){
		page(2);
	}else if(r=='score'){
		page(3);
	}else if(r=='my'){
		page(5);
	}
	$(window).scroll(function(){
		if($(this).scrollTop()<100){
		  if($("#page1").css("display")=='none') return;
            $(".searchNav").show();
        } else {
            $(".searchNav").hide();
        }
	})
})


function scrollOne(){
	var swiper = new Swiper('.one_swiper', {
		autoplay : 3000,
		paginationtouchendable : false,
		pagination : '.one-pagination',
		observer:true,//修改swiper自己或子元素时，自动初始化swiper
		observeParents:true,//修改swiper的父元素时，自动初始化swiper
		autoplayDisableOnInteraction : false,
	});
}

function scrollTwo(){
	var swiper2 = new Swiper('.four_swiper', {
		paginationtouchendable : false,
		pagination : '.four-pagination',
	    observer:true,//修改swiper自己或子元素时，自动初始化swiper
		observeParents:true,//修改swiper的父元素时，自动初始化swiper
		autoplay : 3000,
		autoplayDisableOnInteraction : false,
	});
}


$('#editCart').click(function(){
	var status=$(this).attr('data-status');
	if(status==0){
		$(this).attr('data-status',1);
		$('#editCart').text('取消');
		$('.editCart').find('div').hide();
		$('.showDel').show();
		$('.delSelect').show();
		$('.toPay').hide();
		$('.total_select').hide();
	}else{
		$(this).attr('data-status',0);
		$('#editCart').text('编辑');
		$('.editCart').find('div').show();
		$('.showDel').hide();
		$('.delSelect').hide();
		$('.toPay').show();
		$('.total_select').show();
	}
})

//删除单个
$('.delCartGoods').click(function(){
	  var goodsId=$(this).attr('data-id');
	layer.confirm('确定删除吗?', {icon: 3, title:'删除商品'}, function(index){
			 $.ajax({
		         type: "POST",
		         url: "<?php echo U('Orders/delCartGoods');?>",
		         data: {ids:goodsId},
		         dataType: "json",
		         success: function(data){
		        	 	if(data.status==0){
		        	 		layer.msg('删除成功');;
		        	 		setTimeout(function(){
		        	 			location.href="<?php echo U('Index/index',array('r'=>'car'));?>";
		        	 		})
		        	 	}else{
		        	 		layer.msg('删除失败');
		        	 	}
		           }
		     });
		  layer.close(index);
		});
})


//批量删除所选
$('.delSelect').click(function(){
	var ids='';
	$('.chkall').each(function(){
			if($(this).prop('checked')){
				ids+=$(this).attr('data-gid')+'_'+$(this).attr('dataid')+'_'+$(this).attr('data-isseckill')+'_'+$(this).attr('data-isground')+',';
			}
		})
		ids=ids.substring(0,ids.length-1);
		layer.confirm('确定删除吗?', {icon: 3, title:'删除商品'}, function(index){
			 $.ajax({
			         type: "POST",
			         url: "<?php echo U('Orders/delCartGoods');?>",
			         data: {ids:ids},
			         dataType: "json",
			         success: function(data){
			        	 	if(data.status==0){
			        	 		layer.msg('删除成功');;
			        	 		setTimeout(function(){
			        	 			location.href="<?php echo U('Index/index',array('r'=>'car'));?>";
			        	 		})
			        	 	}else{
			        	 		layer.msg('删除失败');
			        	 	}
			           }
			     });
		  layer.close(index);
		});
})



	function page(page) {
		$('#editCart').hide();
		switch (page) {
		case 1:
			$('title').text('首页');
			$('.searchNav').show();
			$('#page1').show();
			$('#page2').hide();
			$('#page3').hide();
			$('#page4').hide();
			$('#page5').hide();
			$('#page1');
			$('._titles').text('首页');
			$('#one').attr('src', '/Tpl/Wx/image/home_click_2.png');
			$('#two').attr('src', '/Tpl/Wx/image/fenlei_click_1.png');
			$('#three').attr('src', '/Tpl/Wx/image/jifen_click_1.png');
			$('#four').attr('src', '/Tpl/Wx/image/goshop_click_1.png');
			$('#five').attr('src', '/Tpl/Wx/image/my_click_1.png');
			break;
		case 2:
			loaded();
			$('title').text('分类');
			$('.searchNav').hide();
			$('#page2').show();
			$('#page1').hide();
			$('#page3').hide();
			$('#page4').hide();
			$('#page5').hide();
			$('._titles').text('分类');
			$('title').text('分类');
			$('#one').attr('src', '/Tpl/Wx/image/home_click_1.png');
			$('#two').attr('src', '/Tpl/Wx/image/fenlei_click_2.png');
			$('#three').attr('src', '/Tpl/Wx/image/jifen_click_1.png');
			$('#four').attr('src', '/Tpl/Wx/image/goshop_click_1.png');
			$('#five').attr('src', '/Tpl/Wx/image/my_click_1.png');
			break;
		case 3:
			$('title').text('积分乐园');
			$('.searchNav').hide();
			$('#page3').show();
			$('#page1').hide();
			$('#page2').hide();
			$('#page4').hide();
			$('#page5').hide();
			$('._titles').text('积分乐园');
			$('#one').attr('src', '/Tpl/Wx/image/home_click_1.png');
			$('#two').attr('src', '/Tpl/Wx/image/fenlei_click_1.png');
			$('#three').attr('src', '/Tpl/Wx/image/jifen_click_2.png');
			$('#four').attr('src', '/Tpl/Wx/image/goshop_click_1.png');
			$('#five').attr('src', '/Tpl/Wx/image/my_click_1.png');
			break;
		case 4:
			$('title').text('购物车');
			$('.searchNav').hide();
			$('#page4').show();
			$('#page1').hide();
			$('#page3').hide();
			$('#page2').hide();
			$('#page5').hide();
			$('#editCart').show();
			$('._titles').text('购物车');
			$('#one').attr('src', '/Tpl/Wx/image/home_click_1.png');
			$('#two').attr('src', '/Tpl/Wx/image/fenlei_click_1.png');
			$('#three').attr('src', '/Tpl/Wx/image/jifen_click_1.png');
			$('#four').attr('src', '/Tpl/Wx/image/goshop_click_2.png');
			$('#five').attr('src', '/Tpl/Wx/image/my_click_1.png');
			break;
		case 5:
			$('title').text('我的');
			$('.searchNav').hide();
			$('#page5').show();
			$('#page1').hide();
			$('#page3').hide();
			$('#page4').hide();
			$('#page2').hide();
			$('._titles').text('我的');
			$('#one').attr('src', '/Tpl/Wx/image/home_click_1.png');
			$('#two').attr('src', '/Tpl/Wx/image/fenlei_click_1.png');
			$('#three').attr('src', '/Tpl/Wx/image/jifen_click_1.png');
			$('#four').attr('src', '/Tpl/Wx/image/goshop_click_1.png');
			$('#five').attr('src', '/Tpl/Wx/image/my_click_2.png');
			break;
		}

	}


	$('body').on('click','.car_input',function(){
		$('#cateForm').submit();
	});
	$('body').on('click','.gotoSerach',function(){
		$('#indexForm').submit();
	});

	$(function(){
		//没达到起送价

		$('.deliveryFlag').each(function(){
			var id=$(this).attr('data-id');
			var val=parseFloat($(this).val());
			var money=parseFloat($('#prc_'+id).text());
			if(val>money){
				$("#color_"+id).css('background','#4CD964');
			}
		})
	})

var swiper = new Swiper('.swiper-container_secKill', {
	pagination: '.swiper-wrapper_secKill',
	slidesPerView: 3,
	paginationClickable: true,
	spaceBetween: 30,
	freeMode: true
});

	var adSwiper = new Swiper(".Hotswiper-container",{
		pagination:"swiper-wrapper",
		direction: 'vertical',
		slidesPerView: 1,
		paginationClickable: true,
		spaceBetween: 30,
		mousewheelControl: true,
		autoplay : 2000

	});

function goto(event){
var _this= $(event).attr('data-da');
var data = 	_this.split('|');
var type=	data[0];
var goWhere = data[1];

	try{

		switch (type){
			case 'S':
				window.location.href = '/Wx/Shop/shop/id/'+goWhere+'.html';

				break;
			case 'G':
				window.location.href = '/Wx/Index/goodsDetail/id/'+goWhere+'/r/index.html';
				break;
			default:
				if($(event).attr('data-url')!=''){
					window.location.href = $(event).attr('data-url');

				}
				break;
		}




	}catch(w) {

		console.log(w);
	}




}

</script>

<script type="text/javascript">
	$(function() {
		//菜单框架自动获取高度
		//杨 修改分类页面不能上下滑动问题
		//var doc_H=window.screen.height-145;
		var doc_H=$(window).height()-45-51;
		$('.Menu_box').height(doc_H);
		$('#right_Menu').height(doc_H-41);
		$('#left_Menu').height(doc_H-41);
		window.onresize=function(){
			//var doc_H=document.body.clientHeight-145;
			var doc_H=$(window).height()-45-51;
			$('.Menu_box').height(doc_H);
		}
		$('.ot-maininfo').css('padding-bottom','10px');
		
	});
	var myScroll_left;
	var myScroll_right;
	function loaded () {
		setTimeout(function(){
//			myScroll_left = new IScroll('#left_Menu', { mouseWheel: true, click: true });
//			myScroll_right = new IScroll('#right_Menu', { mouseWheel: true, click: true });

//			  $(".ot-maininfo").children(".menutype").hide();
//	          $(".ot-maininfo").children(".menutype").eq(0).show();




//	      	myScroll_left.refresh();
//			myScroll_right.refresh();
		})

	}
	//document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);



	$(document).ready(function() {	//tbl切换

		$($(".ot-menu  li").eq(0)).addClass('active');

		$(".ot-menu  li").click(function(){

			tabIndex = $(this).index();
//			console.log(tabIndex);
//				console.log($(".ot-menu  li").eq(tabIndex));
			$(this).siblings("li").removeClass("active");
			var tab = $($(".ot-menu  li").eq(tabIndex)).addClass('active');
			var id  = tab.attr('data-id');
		});
	});







</script>
<script src="/Tpl/Wx/js/index/vue-app.js"></script>
</html>