wx.ready(function () {
  // 1 判断当前版本是否支持指定 JS 接口，支持批量判断
    // wx.checkJsApi({
      // jsApiList: [
        // 'getNetworkType',
        // 'previewImage'
      // ],
      // success: function (res) {
        // alert(JSON.stringify(res));
      // }
    // });

  // 2. 分享接口
  // 2.1 监听“分享给朋友”，按钮点击、自定义分享内容及分享结果接口
  wx.onMenuShareAppMessage({
      title: '',
      desc: '',
      link: '',
      imgUrl: '',
      trigger: function (res) {
        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
        //alert('用户点击发送给朋友');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
    });

  // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
  wx.onMenuShareTimeline({
      title: '',
      link: '',
      imgUrl: '',
      trigger: function (res) {
        // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
        //alert('用户点击分享到朋友圈');
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
   });

  // 2.3 监听“分享到QQ”按钮点击、自定义分享内容及分享结果接口
    wx.onMenuShareQQ({
      title: '',
      desc: '',
      link: '',
      imgUrl: '',
      trigger: function (res) {
        //alert('用户点击分享到QQ');
      },
      complete: function (res) {
        //alert(JSON.stringify(res));
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
    });
  
  // 2.4 监听“分享到微博”按钮点击、自定义分享内容及分享结果接口
    wx.onMenuShareWeibo({
      title: '',
      desc: '',
      link: '',
      imgUrl: '',
      trigger: function (res) {
        //alert('用户点击分享到微博');
      },
      complete: function (res) {
        //alert(JSON.stringify(res));
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
    });

  // 2.5 监听“分享到QZone”按钮点击、自定义分享内容及分享接口
    wx.onMenuShareQZone({
      title: '',
      desc: '',
      link: '',
      imgUrl: '',
      trigger: function (res) {
        //alert('用户点击分享到QZone');
      },
      complete: function (res) {
        //alert(JSON.stringify(res));
      },
      success: function (res) {
        //alert('已分享');
      },
      cancel: function (res) {
        //alert('已取消');
      },
      fail: function (res) {
        //alert(JSON.stringify(res));
      }
    });
    
    wx.onMenuShareAppMessage(shareData);
    wx.onMenuShareTimeline(shareData);
        
});
wx.error(function (res) {
  //alert(res.errMsg);
});

