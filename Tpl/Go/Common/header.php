<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<title><?php echo $title;?></title>
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/agile/css/agile.layout.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/agile/css/ratchet/css/ratchet.min.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/agile/css/flat/flat.component.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/agile/css/flat/flat.color.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/agile/css/flat/iconline.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/app/css/app.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/layer/skin/layer.css" type="text/css" />
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH; ?>/assets/app/css/pagedialog.css" />
<script type="text/javascript">
  window.Path = {
		  Skin : "<?php echo G_TEMPLATES_STYLE;?>", 
		  Webpath : "<?php echo WEB_PATH;?>",
  };
</script>
