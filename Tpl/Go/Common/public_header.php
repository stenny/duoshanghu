<!DOCTYPE html>
<html>
<head id="Head1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title><?php echo $title;?></title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="<?php echo MOBILE_TPL_PATH;?>/css/comm.css" rel="stylesheet" type="text/css" />
<link href="<?php echo MOBILE_TPL_PATH;?>/css/goods.css" rel="stylesheet" type="text/css" />
<script src="<?php echo MOBILE_TPL_PATH;?>/js/jquery.js" language="javascript" type="text/javascript"></script>
<script src="<?php echo MOBILE_TPL_PATH;?>/js/iscroll5.js" type="text/javascript"></script>
<script src="<?php echo MOBILE_TPL_PATH;?>/js/common.js" type="text/javascript"></script>