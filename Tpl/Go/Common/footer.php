<?php
if($footer == null){
?>
<nav class="bar bar-tab">
  	<a class="tab-item" href="<?php echo U('Wx/Index/index');?>">
    	<span class="tab_menu_icon tab_menu_icon1"></span>
		<span class="tab-label">首页</span>
  	</a>
  	<a class="tab-item<?php if (isset($home_active)){ echo ' '.$home_active;}?>" href="<?php echo U('Index/index');?>">
    	<span class="tab_menu_icon  <?php if(isset($home_active)){ echo ' tab_menu_icon2_selected';}else{?>tab_menu_icon2<?php }?>"></span>
		<span class="tab-label">1元云购</span>
  	</a>
  	<a class="tab-item<?php if (isset($lottery_active)){ echo ' '.$lottery_active;}?>" href="<?php echo U('Index/lottery');?>">
		<span class="tab_menu_icon <?php if(isset($lottery_active)){ echo ' tab_menu_icon3_selected';}else{?>tab_menu_icon3<?php }?>"></span>
    	<span class="tab-label">最新揭晓</span>
  	</a>
  	<a class="tab-item head-shopcart<?php if (isset($cart_active)){ echo ' '.$cart_active;}?>" href="<?php echo U('Cart/cartlist');?>">
    	<span class="tab_menu_icon <?php if(isset($cart_active)){ echo ' tab_menu_icon4_selected';}else{?>tab_menu_icon4<?php }?>"></span>
		<span class="tab-label">购物车</span>
		<em class="footer_goods_num" style="display:none;">0</em>
	</a>
  	<a class="tab-item<?php if (isset($re_active)){ echo ' '.$re_active;}?>" href="<?php echo U('User/userbuylist');?>">
		<span class="tab_menu_icon <?php if(isset($re_active)){ echo ' tab_menu_icon5_selected';}else{?>tab_menu_icon5<?php }?>"></span>
    	<span class="tab-label">我的夺宝</span>
  	</a>
</nav>
<?php }?>
</section>
</div>
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/layer2.1/layer.js"></script>
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/js/common.js"></script>
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/js/base.js"></script>
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/js/iscroll5.js"></script>
<script type="text/javascript">
<?php if(!isset($isScroll)){?>
$(function(){
	$.jqScroll('#flat_article');
});
<?php }?>
var Path = new Object();
Path.Skin="<?php echo MOBILE_TPL_PATH;?>";
Path.M = "<?php echo MODULE_NAME;?>";
Path.Webpath = "<?php echo WEB_URL;?>";
var Base = {head: document.getElementsByTagName("head")[0] || document.documentElement,Myload: function(B, A) {this.done = false;B.onload = B.onreadystatechange = function() {if (!this.done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {this.done = true;A();B.onload = B.onreadystatechange = null;if (this.head && B.parentNode) {this.head.removeChild(B)}}}},getScript: function(A, C) {var B = function() {};if (C != undefined) {B = C}var D = document.createElement("script");D.setAttribute("language", "javascript");D.setAttribute("type", "text/javascript");D.setAttribute("src", A);this.head.appendChild(D);this.Myload(D, B)},getStyle: function(A, B) {var B = function() {};if (callBack != undefined) {B = callBack}var C = document.createElement("link");C.setAttribute("type", "text/css");C.setAttribute("rel", "stylesheet");C.setAttribute("href", A);this.head.appendChild(C);this.Myload(C, B)}};
function GetVerNum() {var D = new Date();return D.getFullYear().toString().substring(2, 4) + '.' + (D.getMonth() + 1) + '.' + D.getDate() + '.' + D.getHours() + '.' + (D.getMinutes() < 10 ? '0': D.getMinutes().toString().substring(0, 1))}
Base.getScript('<?php echo MOBILE_TPL_PATH;?>/js/Bottom.js');
var shareData = {title: "<?php if(!isset($this->share_title)){echo _cfg('web_name');}else{echo $this->share_title;}?>",desc: "<?php if(!isset($this->share_desc)){echo _cfg('web_des');}else{echo $this->share_desc;}?>",link: "<?php if(!isset($this->share_link)){echo _cfg('web_path');}else{echo $this->share_link;}?>",imgUrl: "<?php if(!isset($this->share_img)){echo G_UPLOAD_PATH.'/'._cfg('web_logo');}else{echo $this->share_img;}?>"};
</script>
<?php echo $shareScript;?>