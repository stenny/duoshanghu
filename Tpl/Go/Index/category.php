<include file="Common:header" />
</head>
<body>
		<div id="section_container">
			<section id="index_section" data-role="section" class="active">
				
				<header class="bar bar-nav" id="header">
				  	<a class="icon icon-left-nav pull-left"  href="javascript:;"  onclick="history.go(-1)"></a>
				  	<h1 class="title"><?php echo $title;?></h1>
				</header>		
<article data-role="article" data-scroll="verticle" class="active" style="background:#f5f5f5;top:44px;bottom:50px;">

	<div class="scroller create_goods"  style="background: #f5f5f5;">
	
		<div class="left_cate">
			<ul>
				<?php foreach ($category as $key => $value){ 
					if ($key == 0){
				?>
				<li class="selected" item="scrollerMenu<?php echo $key;?>"><a href="javascript:;" id="<?php echo $value['catId'];?>"><?php echo $value['catName'];?></a></li>
				<?php }else{?>
				<li item="scrollerMenu<?php echo $key;?>"><a href="javascript:;" id="<?php echo $value['catId'];?>"><?php echo $value['catName'];?></a></li>
				<?php }}?>
			</ul>
		</div>

		<div class="right_cate" style="display: none;">
			<div class="cate_scroller">
				<?php foreach ($category as $key => $value){ ?>
				<div class="child_main" id="scrollerMenu<?php echo $key;?>" <?php if ($key==0){echo 'style="display:block"';}else{echo 'style="display:none;"';}?>>
					<div class="cate_main_adver">
						<a href="<?php echo $ads['adURL'];?>"><img src="/<?php echo $ads['adFile'];?>"/></a>
					</div>
					<?php foreach ($value['child'] as $k => $v){ ?>
					<h4><a href="<?php echo U('Index/lists',array('cid' => $v['catId']));?>"><?php echo $v['catName'];?></a></h4>
					<?php foreach ($v['child'] as $kk=>$vv){ ?>
					<div class="three_main">
						<dl onclick="window.location.href='<?php echo U('Index/lists',array('cid' => $v['catId'],'cid2' => $vv['catId']));?>'">
							<dt><a href="<?php echo U('Index/lists',array('cid' => $v['catId'],'cid2' => $vv['catId']));?>"><img src="/<?php echo $vv['cateImg'];?>"/></a></dt>
							<dd><?php echo $vv['catName'];?></dd>
						</dl>
					</div>
					<?php } }?>
				</div>
				
				<?php }?>
				
			</div>
		</div>
		</div>

</article>
<include file="Common:footer" />
<script type="text/javascript">
$(function(){
	var h = $(window).height()-94;
	var w = $(window).width();
	$('.left_cate').height(h);
	$('.right_cate').height(h);
	var obj = $('.left_cate ul li');
	obj.click(function(){
		$(obj).removeClass('selected');
		$(this).addClass('selected');
		var domId = $(this).attr('item');
		$('.child_main').hide();
		$('#'+domId).show();
		$.jqScroll(".right_cate");
	});
	$.jqScroll(".left_cate",false);
	$('.right_cate').width(w-105).show();
	$.jqScroll(".right_cate");
	$(window).resize(function(){
		w = $(window).width();
		$('.right_cate').width(w-105);
	});
});
</script>
</body>
</html>