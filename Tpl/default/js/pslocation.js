﻿!function ($) {
	$.extend({
		_jsonp : {
			scripts : {},
			counter : 1,
			charset : "gb2312",
			head : document.getElementsByTagName("head")[0],
			name : function (callback) {
				var name = "_jsonp_" + (new Date).getTime() + "_" + this.counter;
				this.counter++;
				var cb = function (json) {
					eval("delete " + name),
					callback(json),
					$._jsonp.head.removeChild($._jsonp.scripts[name]),
					delete $._jsonp.scripts[name]
				};
				return eval(name + " = cb"),
				name
			},
			load : function (a, b) {
				var c = document.createElement("script");
				c.type = "text/javascript",
				c.charset = this.charset,
				c.src = a,
				this.head.appendChild(c),
				this.scripts[b] = c
			}
		},
		getJSONP : function (a, b) {
			var c = $._jsonp.name(b),
			a = a.replace(/{callback};/, c);
			return $._jsonp.load(a, c),
			this
		}
	})
}
(jQuery);


var isUseServiceLoc = true; //是否默认使用服务端地址
var provinceHtml = '<div class="content"><div data-widget="tabs" class="m JD-stock" id="JD-stock">'
								+'<div class="mt">'
								+'    <ul class="tab">'
								+'        <li data-index="0" data-widget="tab-item" class="curr"><a href="#none" class=""><em>请选择</em><i></i></a></li>'
								+'        <li data-index="1" data-widget="tab-item" class="curr"><a href="#none" class=""><em>请选择</em><i></i></a></li>'
								+'        <li data-index="2" data-widget="tab-item" class="curr"><a href="#none" class=""><em>请选择</em><i></i></a></li>'
								
								+'    </ul>'
								+'    <div class="stock-line"></div>'
								+'</div>'
								+'<div class="mc" data-area="0" data-widget="tab-content" id="stock_province_item"></div>'
								+'<div class="mc" data-area="1" data-widget="tab-content" id="stock_city_item"></div>'
								+'<div class="mc" data-area="2" data-widget="tab-content" id="stock_area_item"></div>'

								+'</div></div>';
function getAreaList(result,level){
	var html = ["<ul class='area-list'>"];
	var longhtml = [];
	var longerhtml = [];
	if (result&&result.length > 0){
		for (var i=0,j=result.length;i<j ;i++ ){
			result[i].name = result[i].name.replace(" ","");
			if(level == 1){
				if(result[i].name.length > 12){
					longerhtml.push("<li class='longer-area' areatype='1' onclick=chooseCity("+result[i].id+",'"+result[i].name+"')><a href='#none' data-value='"+result[i].id+"'>aa</a></li>");
				}else if(result[i].name.length > 5){
					longhtml.push("<li class='long-area' areatype='1' onclick=chooseCity("+result[i].id+",'"+result[i].name+"')><a href='#none' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}else{
					html.push("<li areatype='1' onclick=chooseCity("+result[i].id+",'"+result[i].name+"')><a href='#none' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}
			}else if(level==2){
				if(result[i].name.length > 12){
					longerhtml.push("<li class='longer-area' areatype='2' onclick=chooseArea("+result[i].id+",'"+result[i].name+"')><a href='#none' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}else if(result[i].name.length > 5){
					longhtml.push("<li class='long-area' areatype='2' onclick=chooseArea("+result[i].id+",'"+result[i].name+"')><a href='#none' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}else{
					html.push("<li areatype='2' onclick=chooseArea("+result[i].id+",'"+result[i].name+"')><a href='#none' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}
			}else{
				if(result[i].name.length > 12){
					longerhtml.push("<li class='longer-area' areatype='3' onclick='chooseTown(this)'><a href='#none' class='wst-town_"+result[i].id+"' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}else if(result[i].name.length > 5){
					longhtml.push("<li class='long-area' areatype='3' onclick='chooseTown(this)'><a href='#none' class='wst-town_"+result[i].id+"' data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}else{
					html.push("<li areatype='3' onclick='chooseTown(this)'><a href='#none' class='wst-town_"+result[i].id+"'data-value='"+result[i].id+"'>"+result[i].name+"</a></li>");
				}
			}
		}
	}else{
		html.push("<li><a href='#none' data-value='"+currentAreaInfo.currentFid+"'> </a></li>");
	}
	html.push(longhtml.join(""));
	html.push(longerhtml.join(""));
	html.push("</ul>");
	return html.join("");
}
function cleanKuohao(str){
	if(str&&str.indexOf("(")>0){
		str = str.substring(0,str.indexOf("("));
	}
	if(str&&str.indexOf("（")>0){
		str = str.substring(0,str.indexOf("（"));
	}
	return str;
}

function getStockOpt(id,name){
	if(currentAreaInfo.currentLevel==3){
		currentAreaInfo.currentAreaId = id;
		currentAreaInfo.currentAreaName = name;
		if(!page_load){
			currentAreaInfo.currentTownId = 0;
			currentAreaInfo.currentTownName = "";
		}
	}else if(currentAreaInfo.currentLevel==4){
		currentAreaInfo.currentAreaId = id;
		currentAreaInfo.currentAreaName = name;
	}

	if(currentAreaInfo.currentLevel==4)$('#store-selector').removeClass('hover');
	//setCommonCookies(currentAreaInfo.currentProvinceId,currentLocation,currentAreaInfo.currentCityId,currentAreaInfo.currentAreaId,currentAreaInfo.currentTownId,!page_load);
	if(page_load){
		page_load = false;
	}
	var address = cleanKuohao(currentAreaInfo.currentProvinceName) + cleanKuohao(currentAreaInfo.currentCityName)+cleanKuohao(currentAreaInfo.currentAreaName);
	areaTabContainer.eq(0).removeClass("curr").find("em").html(cleanKuohao(currentAreaInfo.currentProvinceName));
	$("#store-selector .text div").html(address).attr("title",address).attr("provinceId",currentAreaInfo.currentProvinceId).attr('cityId',currentAreaInfo.currentCityId).attr('areaId',currentAreaInfo.currentAreaId).attr('id','chooseAreas');
}
function getAreaListcallback(r,level){
	if (currentAreaInfo.currentLevel == 1){
		provinceContainer.html(getAreaList(r,level));
		provinceContainer.find("a").bind("click",function(){
			if(page_load){
				page_load = false;
			}
			$("#wst-test").html(provinceContainer.attr("id"));
			currentAreaInfo.currentLevel=2;
			
			//getStockOpt($(this).attr("data-value"),$(this).html());
		});
		if(page_load){ //初始化加载
			currentAreaInfo.currentLevel = currentAreaInfo.currentLevel==1?2:3;
			if(currentAreaInfo.currentAreaId && new Number(currentAreaInfo.currentAreaId)>0){
				getStockOpt(currentAreaInfo.currentAreaId,currentDom.find("a[data-value='"+currentAreaInfo.currentAreaId+"']").html());
			}else{
				getStockOpt(currentDom.find("a").eq(0).attr("data-value"),currentDom.find("a").eq(0).html());
			}
		}
	}else if (currentAreaInfo.currentLevel == 2){
		cityContainer.html(getAreaList(r,level));
		cityContainer.find("a").bind("click",function(){
			if(page_load){
				page_load = false;
			}
			$("#wst-test").html(cityContainer.attr("id"));
			currentAreaInfo.currentLevel=3;
			
			//getStockOpt($(this).attr("data-value"),$(this).html());
		});
		if(page_load){ 
			currentAreaInfo.currentLevel = currentAreaInfo.currentLevel==2?3:4;
			if(currentAreaInfo.currentAreaId && new Number(currentAreaInfo.currentAreaId)>0){
				getStockOpt(currentAreaInfo.currentAreaId,currentDom.find("a[data-value='"+currentAreaInfo.currentAreaId+"']").html());
			}else{
				getStockOpt(currentDom.find("a").eq(0).attr("data-value"),currentDom.find("a").eq(0).html());
			}
		}
	}else if (currentAreaInfo.currentLevel == 3){
		areaContainer.html(getAreaList(r,level));
		areaContainer.find("a").bind("click",function(){
			if(page_load){
				page_load = false;
			}
			$("#wst-test").html(areaContainer.attr("id"));
			
			currentAreaInfo.currentLevel=4;
		
			getStockOpt($(this).attr("data-value"),$(this).html());
		});
		if(page_load){
			currentAreaInfo.currentLevel = currentAreaInfo.currentLevel==3?4:5;
			if(currentAreaInfo.currentAreaId && new Number(currentAreaInfo.currentAreaId)>0){
				getStockOpt(currentAreaInfo.currentAreaId,currentDom.find("a[data-value='"+currentAreaInfo.currentAreaId+"']").html());
			}else{
				getStockOpt(currentDom.find("a").eq(0).attr("data-value"),currentDom.find("a").eq(0).html());
			}
		}
	}
}
//选择省份
function chooseProvince(cityId,cityName){
	var shopId = $("#shopId").val();
	currentAreaInfo.currentLevel = 1;
	currentAreaInfo.currentCityId = cityId;
	currentAreaInfo.currentCityName = cityName;
	if(!page_load){
		currentAreaInfo.currentAreaId = 0;
		currentAreaInfo.currentAreaName = "";
		currentAreaInfo.currentTownId = 0;
		currentAreaInfo.currentTownName = "";
	}
	//栏目名称
	areaTabContainer.eq(0).addClass("curr").show().find("em").html("请选择");
	areaTabContainer.eq(1).removeClass("curr").show().find("em").html("请选择");

	$.post(Think.U('Home/UserAddress/getShopProvince'),{areaId2:0,shopId:shopId},function(data,textStatus){
		var json = WST.toJson(data);
		getAreaListcallback(json,1);
	});
}
//选择城市
function chooseCity(provinceId,provinceName){
	var shopId = $("#shopId").val();
	currentAreaInfo.currentLevel = 2;
	var cityId = currentAreaInfo.currentCityId;
	currentAreaInfo.currentProvinceId = provinceId;
	currentAreaInfo.currentProvinceName = provinceName; 
	if(!page_load){
		currentAreaInfo.currentAreaId = 0;
		currentAreaInfo.currentAreaName = "";
		currentAreaInfo.currentTownId = 0;
		currentAreaInfo.currentTownName = "";
	}
	//栏目名称
	areaTabContainer.eq(0).removeClass("curr").show().find("em").html(provinceName);
	areaTabContainer.eq(1).addClass("curr").show().find("em").html("请选择");
	provinceContainer.hide();
	areaContainer.hide();
	cityContainer.show().html("<div class='iloading'>正在加载中，请稍候...</div>");
	
	$.post(Think.U('Home/UserAddress/getShopCity'),{areaId1:provinceId,shopId:shopId},function(data,textStatus){
		var json = WST.toJson(data);
		getAreaListcallback(json,2);
	});
}

//选择地区/县
function chooseArea(cityId,cityName){
	var shopId = $("#shopId").val();
	currentAreaInfo.currentLevel = 3;
	currentAreaInfo.currentCityId = cityId;
	currentAreaInfo.currentCityName = cityName;
	if(!page_load){
		currentAreaInfo.currentAreaId = 0;
		currentAreaInfo.currentAreaName = "";
		currentAreaInfo.currentTownId = 0;
		currentAreaInfo.currentTownName = "";
	}
	//栏目名称
	areaTabContainer.eq(1).removeClass("curr").show().find("em").html(cityName);
	areaTabContainer.eq(2).addClass("curr").show().find("em").html("请选择");
	cityContainer.hide();
	areaContainer.show();
	$.post(Think.U('Home/UserAddress/getShopDistricts'),{areaId2:cityId,shopId:shopId},function(data,textStatus){
		var json = WST.toJson(data);
		getAreaListcallback(json,3);
	});
}
//选择社区
function chooseTown(obj){
	//obj
}

$("#store-selector .text").after(provinceHtml);
var areaTabContainer=$("#JD-stock .tab li");
var provinceContainer=$("#stock_province_item");//省
var cityContainer=$("#stock_city_item");//市
var areaContainer=$("#stock_area_item");//区
var townaContainer=$("#stock_town_item");//社区
var baseDom = currentDom = areaContainer;//默认开始选择地区
//当前地域信息
var currentAreaInfo;
//初始化当前地域信息
function CurrentAreaInfoInit(){
	var shopId = $("#shopId").val();
	currentAreaInfo =  {"currentLevel": 1,"currentProvinceId": 1,"currentProvinceName":"","currentCityId": 0,"currentCityName":"","currentAreaId": 0,"currentAreaName":"","currentTownId":0,"currentTownName":""};
	var ipLoc = getCookie("ipLoc-djd");
	ipLoc = ipLoc?ipLoc.split("-"):[1,72,0,0];
	$.post(Think.U('Home/UserAddress/getShopProvince'),{areaId2:currCityId,shopId:shopId},function(data,textStatus){
		var json = WST.toJson(data);
		currentAreaInfo.currentProvinceId = json[0]['id'];
		currentAreaInfo.currentProvinceName = json[0]['name'];
	});
	if(ipLoc.length>1&&ipLoc[1]){
		currentAreaInfo.currentCityId = ipLoc[1];
	}
	if(ipLoc.length>2&&ipLoc[2]){
		currentAreaInfo.currentAreaId = ipLoc[3];
	}
	if(ipLoc.length>3&&ipLoc[3]){
		currentAreaInfo.currentTownId = ipLoc[3];
	}
}
var page_load = true;
(function(){
	$("#store-selector").unbind("mouseover").bind("mouseover",function(){
		$('#store-selector').addClass('hover');
		$("#store-selector .content,#JD-stock").show();
	}).find("dl").remove();
	CurrentAreaInfoInit();
	areaTabContainer.eq(0).find("a").click(function(){
		areaTabContainer.removeClass("curr");
		areaTabContainer.eq(0).addClass("curr").show();
		provinceContainer.show();
		cityContainer.hide();
		areaContainer.hide();
		chooseProvince(currCityId,currCityName);
		
	});
	areaTabContainer.eq(1).find("a").click(function(){
		areaTabContainer.removeClass("curr");
		areaTabContainer.eq(1).addClass("curr").show();
		cityContainer.show();
		areaContainer.hide();
		chooseCity(currentAreaInfo.currentProvinceId,currentAreaInfo.currentProvinceName);
	});
	//选择地区
	chooseArea(currCityId,currCityName);
})();

function getCookie(name) {
	var start = document.cookie.indexOf(name + "=");
	var len = start + name.length + 1;
	if ((!start) && (name != document.cookie.substring(0, name.length))) {
		return null;
	}
	if (start == -1) return null;
	var end = document.cookie.indexOf(';', len);
	if (end == -1) end = document.cookie.length;
	return unescape(document.cookie.substring(len, end));
};





