/**
 * Created by zhoucaiguang on 16/8/29.
 */

function getClassData(classId){
    var deferred = Q.defer();
    $.get('/wx/index/getClassData',{
        id:classId
    },function(data,err) {

        if(data){
            deferred.resolve(data);
        }else{
            deferred.reject(data);

        }
    });



    return deferred.promise;
}

(function(){

    //做成异步获取的数据
    Vue.component('child', {
        template: '#child-template',
        ready:function(){
            var init = $($(".ot-menu  li").eq(0));
            var id = init.attr('data-id');
            var url = init.attr('data-url');
            console.log( );
            this.$dispatch('child-msg',id,url);

        },
        methods: {
            addTodo:function(){
                var id = $(event.currentTarget).attr('data-id');
                var url = $(event.currentTarget).attr('data-url');
                this.$dispatch('child-msg',id,url);

                // console.log(event.currentTarget);

            }
        }
    });
 var app =    new Vue({
        el: '#app',
        data: {
            topImg:'',
            rightItems:[]
        },
        methods: {
            upDataRightView: function (event,url) {
                var id= event;
                var _this = this;

                getClassData(id).then(function(data){

                    _this.rightItems=data;
                    _this.topImg=url;
                    // console.log(_this.rightItems);
                });

            },
            removeTodo: function (index) {
                this.todos.splice(index, 1)
            }
        },events:{
            'child-msg':function(msg,url){
                console.log(msg);
                this.upDataRightView(msg,url);
            }
     }
    });




})();

