<?php
namespace Admin\Controller;
class YunorderController extends BaseController {
	
	public function goods(){
		$this->isLogin();
		$this->checkPrivelege('splb_00');
		//获取地区信息
		$m = D('Admin/Areas');
		$this->assign('areaList',$m->queryShowByList(0));
		//获取商品分类信息
		$m = D('Admin/GoodsCats');
		$this->assign('goodsCatsList',$m->queryByList());
		$map = "`q_uid` is null and `q_end_time` is null";
		$shopid = isset($_GET['shopid'])?intval($_GET['shopid']):0;
		if ($shopid){
			$map .= " and `shop_id`=".$shopid;
		}
		
		$shoplistModel = M('shoplist');
		$totalRows = $shoplistModel->where($map)->count();
		$page = new \Think\Page($totalRows,1);
		$limit = $page->firstRow.','.$page->listRows;
		$data = $shoplistModel->where($map)->limit($limit)->select();
		$shopModel = M('shops');
		foreach ($data as $key => $value) {
			$data[$key]['shopName'] = $shopModel->where(array('shopId' => $value['shop_id']))->getField('shopName');
		}
		$this->lists = $data;
		$this->assign('page',$page->show());
		$this->assign('shopName',I('shopName'));
		$this->assign('goodsName',I('goodsName'));
		$this->assign('areaId1',I('areaId1',0));
		$this->assign('areaId2',I('areaId2',0));
		$this->assign('goodsCatId1',I('goodsCatId1',0));
		$this->assign('goodsCatId2',I('goodsCatId2',0));
		$this->assign('goodsCatId3',I('goodsCatId3',0));
		$this->assign('isAdminBest',I('isAdminBest',-1));
		$this->assign('isAdminRecom',I('isAdminRecom',-1));
		$this->display();
	}
	
	public function changeGoodsStatus(){
		$this->isAjaxLogin();
		$rd = array('status'=>-1);
		if (IS_AJAX){
			$tag = intval($_POST['tag']);
			$id = isset($_POST['id'])?intval($_POST['id']):0;
			if (M('shoplist')->where(array('id' => $id))->setField('status',$tag)){
				$rd['status'] = 1;
			}
		}
		$this->ajaxReturn($rd);
	}
	
	public function toView(){
		$this->isLogin();
		$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
		if (!$id){
			$this->error('商品不存在');
		}
		$object = M('shoplist')->where(array('id' => $id))->find();
		$gallery = explode(',', $object['picarr']);
		foreach ($gallery as $key => $value) {
			list($a,$b) = explode('@', $value);
			$gallery[$key] = $b;
		}
		$object['gallery'] = $gallery;
		$this->object = $object;
		$gods = M('goods_cats')->where(array('catId' => array('IN',"$object[mall_cateid],$object[mall_cateid2],$object[mall_cateid3]")))->select();
		$gxt = array();
		foreach ($gods as $key => $value){
			$gxt[$value['catId']] = $value['catName'];
		}
		$this->gods = $gxt;
		$shopCats = M('shops_cats')->where(array('catId' => array('IN',"$object[shop_cateid],$object[shop_cateid2]")))->select();
		$sgxt = array();
		foreach ($shopCats as $key => $value){
			$sgxt[$value['catId']] = $value['catName'];
		}
		$this->shopCats = $sgxt;
		$this->display('view');
	}
	
	public function index(){
		$this->isLogin();
		$this->checkPrivelege('splb_00');		
		$member_go_record = M('member_go_record');
		$TotalRows = $member_go_record->count();
		$Page = new \Think\Page($TotalRows,20);
		$data = $member_go_record->field('goucode',true)->limit($Page->firstRow.','.$Page->listRows)->order('id desc')->select();	
		$this->assign('data',$data);
		$this->assign('page',$Page->show());
		$this->display();
	}
	
	public function zzj(){
		$this->isLogin();
		$this->checkPrivelege('splb_00');
		$member_go_record = M('member_go_record');
		$where = "`huode` != 0";
		$TotalRows = $member_go_record->where($where)->count();
		$Page = new \Think\Page($TotalRows,20);
		$data = $member_go_record->where($where)->field('goucode',true)->limit($Page->firstRow.','.$Page->listRows)->order('id desc')->select();
		$this->assign('data',$data);
		$this->assign('page',$Page->show());
		$this->display();
	}
	
	public function detail(){
		$this->isLogin();
		$id=abs($_GET['id']);
		$model = new \Think\Model();
		$record = $model->query("select * from `__PREFIX__member_go_record` where id=$id limit 1");
		if(!$record){
			$this->error('参数不正确');
		}
		$record = current($record);
		if(isset($_POST['submit'])){
			$record_code =explode(",",$record['status']);
			$status = $_POST['status'];
			$company = $_POST['company'];
			$company_code = $_POST['company_code'];
			$company_money = floatval($_POST['company_money']);
			$code = abs(intval($_POST['code']));
			if(!$company_money){
				$company_money = '0.00';
			}else{
				$company_money = sprintf("%.2f",$company_money);
			}
		
			if($status == '未完成'){
				$status = $record_code[0].','.$record_code[1].','.'未完成';
			}
			if($status == '已发货'){
				$status = '已付款,已发货,待收货';
			}
			if($status == '未发货'){
				$status = '已付款,未发货,未完成';
			}
			if($status == '已完成'){
				$status = '已付款,已发货,已完成';
			}
			if($status == '已作废'){
				$status = $record_code[0].','.$record_code[1].','.'已作废';
			}
			
			$ret = $model->execute("UPDATE `__PREFIX__member_go_record` SET `status`='$status',`company` = '$company',`company_code` = '$company_code',`company_money` = '$company_money' where id='$code'");
			if($ret){
				$this->success("更新成功");
			}else{
				$this->error("更新失败");
			}
			exit;
		}
		$uid= $record['uid'];
		$user = $model->query("select * from `__PREFIX__users` where `userId` = '$uid'");
		$user = current($user);
		$user_dizhi = $model->query("select * from `__PREFIX__user_address` where `userId` = '$uid' and `isDefault` = '1'");
		$go_time = $record['time'];
		$this->record = $record;
		$shopid = $record['shopid'];
		$shop = $model->query("SELECT * FROM `__PREFIX__shoplist` where `id`='$shopid'");
		$shop = current($shop);
		$qishu=array();
		if(!$shop['q_uid']){
			$qishu['ren']='还未开奖';
			$qishu['ma']='还未开奖';
		}else{
			$qishu['ren']=self::get_user_name($user);
			$qishu['ma']=$shop['q_user_code'];
		}
		$this->shop = $shop;
		$this->qishu = $qishu;
		$this->user = $user;
		$this->go_time = $go_time;
		$areas = M('areas');
		$communitys = M('communitys');
		$user_dizhi = current($user_dizhi);
		$user_dizhi['areaId1'] = $areas->where(array('areaId' => $user_dizhi['areaId1']))->getField('areaName');
		$user_dizhi['areaId2'] = $areas->where(array('areaId' => $user_dizhi['areaId2']))->getField('areaName');
		$user_dizhi['areaId3'] = $areas->where(array('areaId' => $user_dizhi['areaId3']))->getField('areaName');
		$user_dizhi['community'] = $communitys->where(array('communityId'=>$user_dizhi['communityId']))->getField('communityName');
		$this->user_dizhi = $user_dizhi;
		$this->display();
		
	}
	
	/**
	 * 获取用户昵称
	 * uid 用户id，或者 用户数组
	 * type 获取的类型, userName,userEmail,userPhone
	 * key  获取完整用户名, sub 截取,all 完整
	 */
	private function get_user_name($uid='',$type='userName',$key='sub'){
		if(is_array($uid)){
			if(isset($uid['userName']) && !empty($uid['userName'])){
				return $uid['userName'];
			}
			if(isset($uid['userEmail']) && !empty($uid['userEmail'])){
				if($key=='sub'){
					$email = explode('@',$uid['userEmail']);
					return $uid['userEmail'] = substr($uid['userEmail'],0,2).'*'.$email[1];
				}else{
					return $uid['userEmail'];
				}
			}
			if(isset($uid['userPhone']) && !empty($uid['userPhone'])){
				if($key=='sub'){
					return $uid['userPhone'] = substr($uid['userPhone'],0,3).'****'.substr($uid['userPhone'],7,4);
				}else{
					return $uid['userPhone'];
				}
			}
			return '';
		}else{
			$uid = intval($uid);
			$info = M('Users')->where(array('userId' => $uid))->field(array('userName','userEmail','userPhone'))->find();
			if(isset($info['userName']) && !empty($info['userName'])){
				return $info['userName'];
			}
	
			if(isset($info['userEmail']) && !empty($info['userEmail'])){
				if($key=='sub'){
					$email = explode('@',$info['userEmail']);
					return $info['userEmail'] = substr($info['userEmail'],0,2).'*'.$email[1];
				}else{
					return $info['userEmail'];
				}
			}
			if(isset($info['userPhone']) && !empty($info['userPhone'])){
				if($key=='sub'){
					return $info['userPhone'] = substr($info['userPhone'],0,3).'****'.substr($info['userPhone'],7,4);
				}else{
					return $info['userPhone'];
				}
			}
			if(isset($info[$type]) && !empty($info[$type])){
				return $info[$type];
			}
			return null;
		}
	}
	
	public function ordermoney(){
		$this->isLogin();
		$where = array('is_pay' => 1);
		$where2 = array('is_cashout' => 1);
		$storeid = isset($_GET['storeid'])?intval($_GET['storeid']):0;
		if ($storeid){
			$storeName = M('shops')->where(array('shopId' => $storeid))->getField('shopName');
			$this->storeName = $storeName;
			$where['storeid'] = $storeid;
			$where2['storeid'] = $storeid;
		}
		$model = M('member_go_record');
		$totalRows = $model->where($where)->count();
		//所有订单总金额
		$AlltotalMoney = $model->where(array('is_pay' => 1))->sum('moneycount');
		
		$totalMoney = $model->where($where)->sum('moneycount');
		$calTotalMoney = $model->where($where2)->sum('moneycount');
		$page = new \Think\Page($totalRows,20);
		$limit = $page->firstRow.','.$page->listRows;
		$lists = $model->where($where)->field(array('goucode'),true)->order('id desc')->limit($limit)->select();
		$shops = M('shops');
		foreach ($lists as $key => $value) {
			$lists[$key]['storeName'] = $shops->where(array('shopId' => $value['storeid']))->getField('shopName');
		}
		$this->AlltotalMoney = $AlltotalMoney;
		$this->totalMoney = $totalMoney;
		$this->Nosettlement = sprintf('%.2f',substr(sprintf('%.3f',$totalMoney-$calTotalMoney), 0,-2));
		$this->calTotalMoney = sprintf('%.2f',substr(sprintf('%.3f',$calTotalMoney), 0,-2));
		
		$this->lists = $lists;
		$this->page = $page->show();
		$this->storeid = $storeid;
		$this->display();
	}

	//结算
	public function cashout(){
		$this->isAjaxLogin();
		$rd = array('status'=>-1);
		$id = (int)I('id',0);
		$storeid = (int)I('storeid',0);
		$moneycount = (int)I('moneycount',0);
		if(0 == $id || 0 == $storeid){$this->ajaxReturn($rd);}
		$shopsModel = M('Shops');
		$rs = $shopsModel->where('shopId = '.$storeid)->setInc('bizMoney',$moneycount);
		if($rs){
			$m = M('member_go_record');
			$rs1 = $m->where('id = '.$id.' and storeid = '.$storeid)->setField('is_cashout',1);
		}
		if($rs1){
			$rd['status'] = 1;
		}
		$this->ajaxReturn($rd);
	}
	
}