<?php
 namespace Admin\Controller;
/**
 * 分销模块控制器
 */
class AgentController extends BaseController{

    public function __construct(){

        parent::__construct();



      

    }

    public function historyOrderIndex(){
        $this->isLogin();
        $m = D('Admin/Areas');
        $this->assign('areaList',$m->queryShowByList(0));
        $m = D('Admin/agent');
        $page = $m->historyOrderqueryByPage();
        $pager = new \Think\Page($page['total'],$page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('Page',$page);
        $this->assign('shopName',I('shopName'));
        $this->assign('orderNo',I('orderNo'));
        $this->assign('areaId1',I('areaId1',0));
        $this->assign('areaId2',I('areaId2',0));
        $this->assign('areaId3',I('areaId3',0));
        $this->assign('orderStatus',I('orderStatus',-9999));


        $this->display("agent/historyOrderList");
    }


    public function historyOrderView(){







        $this->isLogin();
        $m = D('Admin/agent');
        if(I('id')>0){
            $object = $m->historyOrderViewGetDetail();
            $this->assign('object',$object);
        }
        $this->assign('referer',$_SERVER['HTTP_REFERER']);




        $this->display("agent/historyOrderView");
    }
    public function usersIndex(){



        $this->isLogin();
        $this->checkPrivelege('hylb_00');
        $m = D('Admin/agent');
        $page = $m->usersQueryByPage();
        $pager = new \Think\Page($page['total'],$page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('loginName',I('loginName'));
        $this->assign('userPhone',I('userPhone'));
        $this->assign('userEmail',I('userEmail'));
        $this->assign('userType',I('userType',-1));
        $this->assign('Page',$page);



        $this->display("agent/usersList");
    }


    public function usersEdit(){
        $this->isLogin();
        $this->checkPrivelege('hylb_00');
        $m = D('Admin/agent');

        if(IS_POST){
            $result = $m->usersToEdit();
            $this->ajaxReturn($result);
        }else{
            if(I('id')>0){
                $object = $m->usersGet();
                $this->assign('object',$object);
            }
        }


        $this->display('agent/usersEdit');
    }

    public function usersMoreInfo(){

        $m = D('Admin/agent');

        $results = $m->usersMoreResult();



        $this->assign('results',json_encode($results));


        $this->display('agent/usersMoreInfoList');
    }

    /*************************agent_order_log_controller_star*****************************/


    public function revenueLogIndex(){
        $this->isLogin();
        $this->checkAjaxPrivelege('ddlb_00');


        $m = D('Admin/Agent');
        $page = $m->revenueQueryByPage();


        $pager = new \Think\Page($page['total'],$page['pageSize']);
        $page['pager'] = $pager->show();
        // dump($page);

        $this->assign('Page',$page);
        $this->assign('shopName',I('shopName'));

        $this->display("agent/revenueLogList");
    }


    /**
     * 查看订单详情
     */
    public function revenueToView(){
        $this->isLogin();
        $this->checkPrivelege('ddlb_00');
        $m = D('Admin/Orders');
        if(I('id')>0){
            $object = $m->getDetail();
            $this->assign('object',$object);
        }
        $this->assign('referer',$_SERVER['HTTP_REFERER']);
        $this->display("/orders/view");
    }













    /*************************agent_order_log_controller_end*****************************/

/********************agent_order_controller_star**************************/
    public function orderIndex(){

        $this->isLogin();
        //$this->checkAjaxPrivelege('ddlb_00');




        $Agent = D('Admin/Agent');


        $tempreslut =$Agent->orderCheckStatus();
        $page      = $Agent->orderListQuery($tempreslut);

        $pager = new \Think\Page($page['total'],$page['pageSize']);
        $page['pager'] = $pager->show();
        $this->assign('Page',$page);


        $this->display("agent/orderList");
    }


    public function orderAction(){

        $this->isLogin();

        $Agent = D('Admin/Agent');

        $rs = $Agent->orderActionReturnMoney();

        $this->ajaxReturn($rs);
    }





    /**
     * 查看订单详情
     */
    public function orderToView(){
        $this->isLogin();
        $this->checkPrivelege('ddlb_00');
        $m = D('Admin/Agent');
        if(I('id')>0){
            $object = $m->orderGetDetail();

            $this->assign('object',$object);
        }
        $this->assign('referer',$_SERVER['HTTP_REFERER']);
        $this->display("/agent/orderView");
    }


    public function text(){

        $uid='123';
        $ee=_encrypt($uid);
        dump($ee);

        $rrr=_encrypt($ee,'DECODE');
        dump($rrr);


        //_setcookie('agent',null);
       //    echo  _getcookie('agent');
    }



    public function  textfun(){
        $re=D('Admin/Agent')->orderCheckopen();


        if($re['status']=='1'){


            $rs=0;
            if((int)I('id',0)>0){
                $orderId=(int)I('id',0);
                $rs = D('Admin/Agent')->orderAgentAction($orderId);
            }
        }else{
            $rs = -1;
        }
        //  $this->display();
        $this->ajaxReturn($rs);
    }


    //计算出来的商品数量*商品价格 不能转换成 浮点型
    public function toAgentOrdersAction($orderId = 25){
        $list=array();
        echo '<meta charset="UTF-8">';
        $order = D('Admin/Agent')->orderCountPrice($orderId);


        $orderInfo = D('Admin/Agent')->orderInfo($orderId);
        $agentset  = D('Admin/Agent')->orderAgentSetInfo();
        $var=explode("|",$agentset['agentProportion']);

        //  dump($order);



        $user = D('Admin/Agent')->orderUserInfo($orderInfo['partnerId']);
        //    $star=M()->startTrans;
        foreach($order as $key2=>$value2){
            foreach($user as $key=>$value){
                $list[$key2][$key] = $value2;
                $list[$key2][$key] = array_merge($list[$key2][$key],$value);
                $addtime['addTime'] = time();
                $list[$key2][$key] = array_merge($list[$key2][$key],$addtime);

                $agentCount['agentCount'] =(string)(($var[$key]*0.01)*$list[$key2][$key]['agentPrice']);
                $list[$key2][$key] = array_merge($list[$key2][$key],$agentCount);

                $agentAdminProportion['agentAdminProportion'] = $var[$key];

                $list[$key2][$key] = array_merge($list[$key2][$key],$agentAdminProportion);

                $whatOk=M('agentRevenueLog')->add($list[$key2][$key]);


                if($whatOk){

                    M('users')->where(array('userId'=>$list[$key2][$key]['userId']))->setInc('agentTotalPrice',$agentCount['agentCount']);
                    //   echo M()->_sql();
                }else{
                    M()->rollback();
                    return false;
                }




            }
        }







        return true;
    }


    private function getMenuTree($arrCat,$parent_id = 0,$level = 0,$agentLevel = 0){
        static  $arrTree = array(); //使用static代替global
        if( empty($arrCat)) return FALSE;
        $level++;
        foreach($arrCat as $key => $value)
        {
            if($value['partnerId'] == $parent_id)
            {
                $value['level'] = $level;
                $arrTree[] = $value;
                unset($arrCat[$key]); //注销当前节点数据，减少已无用的遍历
                if($level<$agentLevel){
                    self::getMenuTree($arrCat,$value[ 'userId'],$level,$agentLevel);
                }

            }
        }

        return $arrTree;
    }


    /********************agent_order_controller_end**************************/










/**************agent_apply_controller_star********************/
    //分销模块申请提现列表
    public function applyIndex(){

        $m= D('Admin/Agent');

        $applyData=$m->applyList();




        $this->assign('applyLog',json_encode($applyData));
//        $this->display("agentapply/list");
        $this->display("agent/applyList");
    }


    //分销模块申请提现操作
    public function applyEdit(){
        $m = D('Admin/Agent');

        $data=$m->applyCheckEdit();





        $this->ajaxReturn($data);
    }


/**************agent_apply_controller_end********************/



    /*agent_setting_controller_star*/
    //分销设置列表
    public function settingIndex(){
        $this->isLogin();
        $this->checkPrivelege('sqlb_00');
        //获取地区信息
        $m = D('Admin/Agent');
        $page = $m->settingQueryByPage();
        $pager = new \Think\Page($page['total'],$page['pageSize']);// 实例化分页类 传入总记录数和每页显示的记录数
        $page['pager'] = $pager->show();
        $this->assign('Page',$page);
        $this->display("/agent/settingList");
    }


    /**
     * 显示分销模块是否显示/隐藏
     */
    public function settingEditIsShow(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('sqlb_02');
        $m = D('Admin/Agent');
        $rs = $m->settingEditIsStatus();
        $this->ajaxReturn($rs);
    }


    /**
     * 新增/修改操作
     */
    public function settingEdit(){
        $this->isAjaxLogin();
        $m = D('Admin/Agent');

        $this->checkAjaxPrivelege('sqlb_02');
        $rs = $m->settingEdit();
        $this->ajaxReturn($rs);
    }





    /**
     * 跳到新增/编辑页面
     */
    public function settingtoEdit(){
        $this->isLogin();
        $m = D('Admin/Agent');
        $object = array();

        $this->checkPrivelege('sqlb_02');
        $object = $m->settingGet();

        $this->assign('object',$object);
        $this->view->display('/agent/settingEdit');
    }
    /*agent_setting_controller_end*/
};
?>
