<?php
return array(
	'VIEW_PATH' => './Tpl/',
	'DEFAULT_THEME' => 'Go',
	'TMPL_TEMPLATE_SUFFIX'  =>  '.php',     // 默认模板文件后缀
	'URL_MODEL' => 0,
	//商品图片地址
	'PIC_URL' => 'http://'.$_SERVER['HTTP_HOST'].'/',
	'WEB_URL' => 'http://'.$_SERVER['HTTP_HOST'],
	'GOODS_END_TIME' => 120,
	'SEND_TYPE' => 10,

	//成功提示页
	'TMPL_ACTION_SUCCESS' => 'Common:jump',
	//失败提示页
	'TMPL_ACTION_ERROR' => 'Common:jump',
	
	//微信 支付配置
	'WxPayConf_pub'=>array(
			'APPID' => 'wxc90f1726e1563446',
			'MCHID' => '1282208201',
			'KEY' => '20151231GZRHchenbiyan20140817LyH',
			'APPSECRET' => 'a6bf07cff3f5661771dc10d292cd35d4',
			'JS_API_CALL_URL' => WEB_HOST.'/wx/wxpay/index/',
			'SSLCERT_PATH' => WEB_HOST.'/ThinkPHP/Library/Vendor/WxPayPubHelper/cacert/apiclient_cert.pem',
			'SSLKEY_PATH' => WEB_HOST.'/ThinkPHP/Library/Vendor/WxPayPubHelper/cacert/apiclient_key.pem',
			'NOTIFY_URL' =>  WEB_HOST.'/index.php/Wx/Wxpay/wxCallBack',
			'CURL_TIMEOUT' => 30
	)
);