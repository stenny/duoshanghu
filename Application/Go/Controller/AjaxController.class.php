<?php
namespace Go\Controller;
class AjaxController extends BaseController {
	
	private $Mcartlist = null;
	
	protected function _initialize(){
		parent::_initialize();
		$this->Mcartlist = json_decode(stripslashes(cookie('Cartlist')),true);
	}
	
	//首页倒计获得商品
	public function get_shop_info(){
		$goods_id = intval($_GET['gid']);
		if ($goods_id){
			$model = M('shoplist');
			$model->where(array('id' => $goods_id))->setField('q_showtime','N');
			$data = $model->where(array('id' => $goods_id))->find();
			$data['username'] = get_user_name($data['q_uid']);
			$data['status']  =1;
			echo json_encode($data);
		}else{
			echo json_encode(array("status" => 0));
		}
	}
	
	//查看计算结果
	public function getCalResult(){
		$itemid= intval($_GET['gid']);
		$curtime=time();
		$model = new \Think\Model();
		$item=$model->query("select * from `__PREFIX__shoplist` where `id`='$itemid' and `q_end_time` is not null LIMIT 1");
		$item = current($item);
		if($item['q_content']){
			$item['contcode']=0;
			$item['itemlist'] = unserialize($item['q_content']);
			foreach($item['itemlist'] as $key=>$val){
				$item['itemlist'][$key]['time']	=microt($val['time']);
				$h=date("H",$val['time']);
				$i=date("i",$val['time']);
				$s=date("s",$val['time']);
				list($timesss,$msss) = explode(".",$val['time']);
				$item['itemlist'][$key]['timecode']=$h.$i.$s.$msss;
			}
	
		}else{
			$item['contcode']=1;
		}
	
		if(!empty($item)){
			$item['code']=0;
	
		}else{
			$item['code']=1;
		}
	
		echo json_encode($item);
		
	}
	
	public function show_get_lottery(){
		$str = $_GET['ids'];
		$str = substr($str, 0,-1);
		$arr = '';
		if (!empty($str)){
			$arr = explode('_', $str);
		}
		$where = array('q_showtime' => 'Y','q_end_time' => array('exp','is not null'));
		if (!empty($arr)){
			$ids = implode(',', $arr);
			$where['id'] = array('NOT IN',$ids);
		}
		//即将揭晓商品
		$data = M('shoplist')->where($where)->find();
		if (empty($data)){
			$data["status"] = 0;
		}else{
			$data['username'] = get_user_name($data['q_uid']);
			$data["status"] = 1;
			$data["q_end_time"] = intval($data['q_end_time']-time());
		}
		echo json_encode($data);
	}
	
	public function delCartItem(){
		$ShopId= intval($_GET['gid']);
		$cartlist=$this->Mcartlist;
		if($ShopId==0){
			$cart['code']=1;   //删除失败
		}else{
			if(is_array($cartlist)){
				if(count($cartlist)==1){
					foreach($cartlist as $key=>$val){
						if($key==$ShopId){
							$cart['code']=0;
							cookie('Cartlist',null);
						}else{
							$cart['code']=1;
						}
					}
				}else{
					foreach($cartlist as $key=>$val){
						if($key==$ShopId){
							$cart['code']=0;
						}else{
							$Mcartlist[$key]['num']=$val['num'];
						}
					}
					cookie('Cartlist',json_encode($Mcartlist));
				}
			}else{
				$cart['code']=1;   //删除失败
			}
		}
		echo json_encode($cart);
	}
	
	//添加购物车
	public function addShopCart(){
		$ShopId= intval($_GET['gid']);
		$ShopNum= intval($_GET['num']);
		$cartbs= I('tag','item');//标识购物车 cart
		$shopis=0;//0表示不存在  1表示存在
		$Mcartlist=$this->Mcartlist;
		if($ShopId==0 || $ShopNum==0){
			$cart['code']=1;   //表示添加失败
		}else{
			if(is_array($Mcartlist)){
				foreach($Mcartlist as $key=>$val){
					if($key==$ShopId){
						if(isset($cartbs) && $cartbs=='cart'){
							$Mcartlist[$ShopId]['num']=$ShopNum;
						}else{
							$Mcartlist[$ShopId]['num']=$val['num']+$ShopNum;
						}
						$shopis=1;
					}else{
						$Mcartlist[$key]['num']=$val['num'];
					}
				}
	
			}else{
				$Mcartlist =array();
				$Mcartlist[$ShopId]['num']=$ShopNum;
			}
	
			if($shopis==0){
				$Mcartlist[$ShopId]['num']=$ShopNum;
			}
			cookie('Cartlist',json_encode($Mcartlist));
			$cart['code']=0;   //表示添加成功
		}
		$cart['num']	=	count($Mcartlist);    //表示现在购物车有多少条记录
		if($cart['num'] == 0){
			$cartkey="123";
		}
		echo json_encode($cart);
	}
	
	//购物车数量
	public function cartnum(){
		$Mcartlist=$this->Mcartlist;
		if(is_array($Mcartlist)){
			$cartnum['code']=0;
			$cartnum['num'] = count($Mcartlist);
		}else{
			$cartnum['code']=1;
			$cartnum['num']=0;
		}
		echo json_encode($cartnum);
	}
	
	public function lottery_shop_set(){
		if(isset($_POST['lottery_sub']) && IS_AJAX){
			$db = new \Think\Model();
			$times = (int)C('GOODS_END_TIME');
			$gid = isset($_POST['gid']) ? abs(intval($_POST['gid'])) : exit();
			$info = $db->query("select id,xsjx_time,thumb,title,q_uid,q_user,q_end_time,money,q_user_code from `__PREFIX__shoplist` where `id` ='$gid'");
			$info = current($info);
			if(!$info || empty($info['q_end_time'])){
				echo '0';exit;
			}
			if($info['xsjx_time']){$info['q_end_time'] = $info['q_end_time']+$times;}
			$times =  str_ireplace(".","",$info['q_end_time']);
			$q_time = substr($info['q_end_time'],0,10);
			$db->execute("update `__PREFIX__shoplist` SET `q_showtime` = 'N' where `id` = '$gid' and `q_showtime` = 'Y' and `q_uid` is not null");
			echo '1';
		}
	}
	
	public function test(){
		p($this->Mcartlist);
	}
	
}