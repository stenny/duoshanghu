<?php
namespace Home\Model;
/**
 * 区域服务类
 */
class AreasModel extends BaseModel {
	/**
	  * 获取店铺的服务县区 
	  */ 
	public function getDistrictsByShop($obj){
		$shopId = $obj["shopId"];
	 	$sql = "SELECT ar.areaId , ar.areaName, sa.shopId 
			 	FROM __PREFIX__areas ar , __PREFIX__shops_communitys sa 
			 	WHERE  ar.areaId = sa.areaId3 AND sa.shopId = $shopId AND sa.communityId>0 GROUP BY areaId";
	 	return $this->query($sql);
	 }
	
	/**
	  * 获取店铺的社区
	  */
 	public function getShopCommunitys($obj){
 		$shopId = $obj["shopId"];
 		$areaId2 = $obj["areaId2"];
	 	$sql = "SELECT c.communityId id, c.communityName name
	 			FROM __PREFIX__communitys c , __PREFIX__shops_communitys sa 
	 			WHERE  c.communityId = sa.communityId AND sa.shopId = $shopId AND sa.communityId>0 Order by communitySort";
	 	$communitys = array();
	 	$communitys[$areaId2] = $this->query($sql);
	 	return $communitys;
	 }
	 
	 /**
	  * 获取列表[带社区]
	  */
	  public function queryAreaAndCommunitysByList($parentId){
	     $m = M('areas');
		 $rs = $m->where('areaFlag=1 and parentId='.$parentId)->field('areaId,areaName')->select();
		 if(count($rs)>0){
		 	$m = M('communitys');
		 	foreach ($rs as $key =>$v){
		 		$r = $m->where('communityFlag=1 and areaId3='.$v['areaId'])->field('communityId,communityName')->select();
		 		if(!empty($r))$rs[$key]['communitys'] = $r;
		 	}
		 }
		 return $rs;
	  }
     /**
	 * 获取区域信息
	 */
	 public function getArea($areaId){
	  	 $m = M('areas');
		 return $m->where('areaFlag=1 and isShow=1 and areaId='.$areaId)->find();
	 }
	  
	/**
	  * 获取城市下的区
	  */
	  public function getDistricts($parentId){
		 return $this->cache('WST_CACHE_CITY_003_'.$parentId,31536000)->where('areaFlag=1 and isShow=1 and parentId='.$parentId)->field('areaId,areaName')->order('parentId, areaSort')->select();
	  }
	    
	  /**
	   * 获取省份列表
	   */
	  public function getProvinceList(){
	  	$rs = array();
	  	$rslist = $this->cache('WST_CACHE_CITY_001',31536000)->where('isShow=1 AND areaFlag = 1 AND areaType=0')->field('areaId,areaName')->order('parentId, areaSort')->select();
	  	foreach ($rslist as $key =>$row){
	  		$rs[$row["areaId"]] = $row;
	  	}
	  	return $rs;
	  }
	  /**
	   * 通过城市获取省份
	   */
	  public function getProvinceByCity($areaId2){
	  	$rs = array();
	  	$parentId = $this->where('isShow = 1 AND areaType = 1 AND areaFlag = 1 AND areaId = '.$areaId2)->getField('parentId');
	  	$rs = $this->where('isShow=1 AND areaFlag = 1 AND areaType=0 AND areaId = '.$parentId)->field('areaId,areaName')->find();
	  	return $rs;
	  }
	  
	  /**
	   * 获取所有城市-根据字母分类
	   */
	  public function getCityGroupByKey(){
	  	$rs = array();
	  	$rslist = $this->cache('WST_CACHE_CITY_000',31536000)->where('isShow=1 AND areaFlag = 1 AND areaType=1')->field('areaId,areaName,areaKey')->order('areaKey, areaSort')->select();
	  	foreach ($rslist as $key =>$row){
	  		$rs[$row["areaKey"]][] = $row;
	  	}
	  	return $rs;
	  }
	  
	  /**
	   * 通过省份获取城市列表
	   */
	  public function getCityListByProvince($provinceId = 0){
	  	$rs = array();
	  	$rslist = $this->cache('WST_CACHE_CITY_002_'.$provinceId,31536000)->where('isShow=1 AND areaFlag = 1 AND areaType=1 AND parentId='.$provinceId)->field('areaId,areaName')->order('parentId, areaSort')->select();
	  	foreach ($rslist as $key =>$row){
	  		$rs[] = $row;
	  	}
	  	return $rslist;
	  }
	  
	  /**
	   * 定位所在城市
	   */
	  public function getDefaultCity(){
	  	$areaId2 = (int)I('city',0);
	  	if($areaId2==0){
	  		$areaId2 = (int)session('areaId2');
	  	}

	  	//检验城市有效性
	  	if($areaId2>0){
	  		$m = D('Home/Areas');
	  		$sql ="SELECT areaId FROM __PREFIX__areas WHERE isShow=1 AND areaFlag = 1 AND areaType=1 AND areaId=".$areaId2;
	  		$rs = $m->query($sql);
	  		if($rs[0]['areaId']=='')$areaId2 = 0;
	  	}else{
	  		$areaId2 = (int)$_COOKIE['areaId2'];
	  	}
	  	//定位城市
	  	if($areaId2==0){
	  		//IP定位
	  		$Ip = new \Org\Net\IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
	  		$area = $Ip->getlocation(get_client_ip());
	  		if($area['area']!=""){
	  			$m = D('Home/Areas');
	  			$sql ="SELECT areaId FROM __PREFIX__areas WHERE isShow=1 AND areaFlag = 1 AND areaType=1 AND areaName like '$cityName'";
	  			$rs = $m->query($sql);
	  			if($rs[0]["areaId"]>0){
	  				$areaId2 = $rs[0]["areaId"];
	  			}else{
	  				$areaId2 = $GLOBALS['CONFIG']['defaultCity'];
	  			}
	  		}else{
	  			$areaId2 = $GLOBALS['CONFIG']['defaultCity'];
	  		}
	  	}
	  	session('areaId2',$areaId2);
	  	setcookie("areaId2", $areaId2, time()+3600*24*90);
	  	return $areaId2;
	  
	  }

	/****************************商品配送渔区*************************/
	public function queryShowByList($parentId){
	    $m = M('areas');
		return $m->where('areaFlag=1 and isShow = 1 and parentId='.(int)$parentId)->select();
	}
	//修改配送区域
	public function saveSaleArea($shopId){
		$m = M('GoodsAreas');
		$areaId1 = (int)I('areaId1',0);
		if($areaId1 > 0){
			$areaId2 = (int)I('areaId2',0);
			if($areaId2 > 0){
				$areaId3 = I('areaId3');
			}else{
				$areaId3 = 0;
			}
		}else{
			$areaId2 = 0;
			$areaId3 = 0;
		}
		$data = array();
		$data['shopId'] = $shopId;
		$data['areaId1'] = $areaId1;
		$data['areaId2'] = $areaId2;
		$data['areaId3'] = implode('-',$areaId3)!=0?implode('-',$areaId3):0;
		$goodsSaleAreaList = cookie('goodsSaleArea');
		cookie('goodsSaleArea',null);
		//删除重复或覆盖的地域
		foreach($goodsSaleAreaList as $k=>$v){
			$area = explode('&',$v);
			if($data['areaId1'] > 0){
				if($data['areaId1'] == $area[1] && ($data['areaId2'] == $area[2] || $data['areaId2'] == 0 || $area[2] == 0)){
					unset($goodsSaleAreaList[$k]);
				}
				if($area[1] == 0){
					unset($goodsSaleAreaList[$k]);
				}
			}else{
				unset($goodsSaleAreaList);
			}
		}
		//加入cookie中
		$goodsSaleAreaList[] = implode('&',$data);
		cookie('goodsSaleArea',$goodsSaleAreaList);
		//获取cookie中的地址名
		$areasData = array();
		$aa = array();
		foreach ($goodsSaleAreaList as $k => $v) {
			$area = explode('&',$v);
			unset($area[0]);
			if(!in_array($area[1],$aa)){
				$aa[] = $area[1];
				$areasData[$area[1]]['area1'] = $area[1];
			}
			$as = array();
			$as['area2'] = $area[2];
			$as['area3'] = $area[3];
			$areasData[$area[1]]['area23'][] = $as;
		}
		$m = M('Areas');
		foreach($areasData as $k => $v){
			$areass = array();
			
			$areaId3 = array();
			
			$areass['area1'] = $m->where('areaId = '.$v['area1'])->getField('areaName');
			foreach ($v['area23'] as $ks => $vs) {
				$areass['area23'][$ks]['area2'] =  $m->where('areaId = '.$vs['area2'])->getField('areaName');
				$areaId3 = explode('-',$vs['area3']);
				$area3 = array();
				$area3Data = array();
				foreach($areaId3 as $kk=>$vv){
					$area3 = $m->where('areaId in('.$vv.')')->getField('areaName');
					$area3Data[] = $area3;
				}
				$areass['area23'][$ks]['area3'] = implode(',',$area3Data);
			}
			$areasData[$k] = $areass;
		}
		return $areasData;
	}
	//删除配送区域
	public function deleteArea(){
		$areaId1 = (int)I('areaId1',0);
		if($areaId1 == 0){return false;}
		$goodsSaleAreaList = cookie('goodsSaleArea');
		$areaData = array();
		foreach($goodsSaleAreaList as $k=>$v){
			$area = explode('&',$v);
			if($area[1] == $areaId1){
				unset($goodsSaleAreaList[$k]);
			}
		}
		cookie('goodsSaleArea',$goodsSaleAreaList);
		//获取cookie中的地址名
		$areasData = array();
		$aa = array();
		foreach ($goodsSaleAreaList as $k => $v) {
			$area = explode('&',$v);
			unset($area[0]);
			if(!in_array($area[1],$aa)){
				$aa[] = $area[1];
				$areasData[$area[1]]['area1'] = $area[1];
			}
			$as = array();
			$as['area2'] = $area[2];
			$as['area3'] = $area[3];
			$areasData[$area[1]]['area23'][] = $as;
		}
		$m = M('Areas');
		foreach($areasData as $k => $v){
			$areass = array();
			
			$areaId3 = array();
			
			$areass['area1'] = $m->where('areaId = '.$v['area1'])->getField('areaName');
			foreach ($v['area23'] as $ks => $vs) {
				$areass['area23'][$ks]['area2'] =  $m->where('areaId = '.$vs['area2'])->getField('areaName');
				$areaId3 = explode('-',$vs['area3']);
				$area3 = array();
				$area3Data = array();
				foreach($areaId3 as $kk=>$vv){
					$area3 = $m->where('areaId in('.$vv.')')->getField('areaName');
					$area3Data[] = $area3;
				}
				$areass['area23'][$ks]['area3'] = implode(',',$area3Data);
			}
			$areasData[$k] = $areass;
		}
		return $areasData;
	}

	//获取商品配送范围
	function getGoodsArea($id){
		cookie('goodsSaleArea',null);
		$m = M('GoodsAreas');
		$goodsAreas = $m->where('goodsId = '.$id)->select();
		$areasData = $this->changeGoodsArea($goodsAreas);
		return $areasData;
	}
	function changeGoodsArea($goodsAreas){
		$data = array();
		foreach($goodsAreas as $k=>$v){
			if(0 != $v['areaId2']){
				if(isset($data[$v['areaId2']])){
					$data[$v['areaId2']]['areaId3'] .= '-'.$v['areaId3'];
				}else{
					$data[$v['areaId2']]['shopId'] = $v['shopId'];
					$data[$v['areaId2']]['areaId1'] = $v['areaId1'];
					$data[$v['areaId2']]['areaId2'] = $v['areaId2'];
					$data[$v['areaId2']]['areaId3'] = $v['areaId3'];
				}
			}else{
				if(isset($data[$v['areaId1']])){
					$data[$v['areaId1']]['areaId3'] .= '-'.$v['areaId3'];
				}else{
					$data[$v['areaId1']]['shopId'] = $v['shopId'];
					$data[$v['areaId1']]['areaId1'] = $v['areaId1'];
					$data[$v['areaId1']]['areaId2'] = $v['areaId2'];
					$data[$v['areaId1']]['areaId3'] = $v['areaId3'];
				}
			}
		}
		$goodsAreas = array();
		foreach($data as $k=>$v){
			$goodsAreas[] = implode('&',$v);
		}
		cookie('goodsSaleArea',$goodsAreas);
		$areasData = array();
		$aa = array();
		foreach ($goodsAreas as $k => $v) {
			$area = explode('&',$v);
			unset($area[0]);
			if(!in_array($area[1],$aa)){
				$aa[] = $area[1];
				$areasData[$area[1]]['area1'] = $area[1];
			}
			$as = array();
			if(0 != $area[2]){
				$as['area2'] = $area[2];
				$as['area3'] = $area[3];
			}
			$areasData[$area[1]]['area23'][] = $as;
		}
		$m = M('Areas');
		foreach($areasData as $k => $v){
			$areass = array();
			$areaId3 = array();
			$areass['area1'] = $m->where('areaId = '.$v['area1'])->getField('areaName');
			foreach ($v['area23'] as $ks => $vs) {
				if(!empty($vs)){
					$areass['area23'][$ks]['area2'] =  $m->where('areaId = '.$vs['area2'])->getField('areaName');
					$areaId3 = explode('-',$vs['area3']);
					$area3 = array();
					$area3Data = array();
					foreach($areaId3 as $kk=>$vv){
						$area3 = $m->where('areaId in('.$vv.')')->getField('areaName');
						$area3Data[] = $area3;
					}
					$areass['area23'][$ks]['area3'] = implode(',',$area3Data);
				}else{
					$areass['area23'] = 0;
				}
			}
			$areasData[$k] = $areass;
		}
		return $areasData;
	}

	function getGoodsAreas(){
		$rd = array('status'=>-1);
		$goodsId = intval(I('goodsId',0));
		$shopId = intval(I('shopId',0));
		$provinceId = (int)I('provinceId',0);
		$cityId = (int)I('cityId',0);
		$areaId = (int)I('areaId',0);
		if(0 == $goodsId || 0 == $shopId){return $rd;}
		$goodsModel = M('Goods');
		$isNation=M('shops')->where(array('shopId'=>$shopId))->getField('isNation');
        //非全国配送时检查配送范围
        if($isNation==0){
            $userArea = array('areaId1'=>$provinceId,'areaId2'=>$cityId,'areaId3'=>$areaId);
            $goodsArea = M("goods_areas")->field('areaId1,areaId2,areaId3')->where(array('goodsId'=>$goodsId,'shopId'=>$shopId))->select();
            $checkResult = $this->checkGoodsArea($goodsArea,$userArea);
            if($checkResult){
                $rd['status'] = 1;
            }
        }else{
        	$rd['status'] = 1;
        }
        return $rd;
	}

	/**
	 * @param $goodsArea
	 * @param $userArea
	 * @return bool 返回true配送范围,返回false 不在配送范围
	 */
	function checkGoodsArea($goodsArea,$userArea){
	    $res = false;
	    foreach ($goodsArea as $gKey=>$gvalue){

	        //全国配送
	        if($gvalue['areaId1']==0&&$gvalue['areaId2']==0){
	            $res = true;
	            break;
	        }elseif($gvalue['areaId1']==$userArea['areaId1'] && $gvalue['areaId2']==0 && $gvalue['areaId3']==0){
	            $res = true;
	            break;
	            //全市配送
	        }elseif ($gvalue['areaId1']==$userArea['areaId1'] && $gvalue['areaId2']==$userArea['areaId2'] && $gvalue['areaId3']==0){
	            $res = true;
	            break;
	            //区域配送
	        }elseif ($gvalue['areaId1']==$userArea['areaId1'] && $gvalue['areaId2']==$userArea['areaId2'] && $gvalue['areaId3']==$userArea['areaId3']){
	            $res = true;
	            break;
	        }


	    }
	    return $res;
	}
}