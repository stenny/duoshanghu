<?php
namespace Home\Controller;
/**
 * 用户账户控制器
 */
class UsersAccountController extends BaseController {
    /**
     * 跳去我的余额页面
     */
	public function queryYue(){
		$this->isUserLogin();
		$USER = session('WST_USER');
		$obj['userId'] = $USER['userId'];
		$usersModel = D('UsersAccount');
		//获取用户余额
		$userMoney = $usersModel->getYue($obj);
		$this->assign('userMoney',$userMoney);
		//获取用户收支明细
		$pages = $usersModel->getPayRecord($obj);
		//分页
        $pager = new \Think\Page($pages['total'],$pages['pageSize']);
        $pages['pager'] = $pager->show();
		$this->assign('pages',$pages);
		$this->assign('mark',I('mark',0));
		$this->assign('time',I('time',0));
		$this->assign("umark","queryYue");
		$this->display('users/usersaccount/yue_list');
	}
	/**
     * 跳去我的积分页面
     */
	public function queryIntegral(){
		$this->isUserLogin();
		$USER = session('WST_USER');
		$obj['userId'] = $USER['userId'];
		$usersModel = D('UsersAccount');
		//获取用户积分
		$userScore = $usersModel->getIntegral($obj);
		$this->assign('userScore',$userScore);
		//获取用户积分消耗记录
		$pages = $usersModel->exchangeRecord($obj);
		$this->assign('pages',$pages);
		$this->assign("umark","queryIntegral");
		$this->display('users/usersaccount/integral_list');
	}
	/*
	*跳去充值页面
	*/
	public function toCharge(){
		$this->isUserLogin();
		$USER = session('WST_USER');
		$obj['userId'] = $USER['userId'];
		//用户信息
		$usersModel = D('Users');
		$user = $usersModel->get($obj['userId']);
		$this->assign('user',$user);
		//充值方式
		$pm = D('Home/Payments');
		$payments = $pm->getList();
		$this->assign("payments",$payments);
		$this->assign("umark","queryYue");
		$this->display('users/usersaccount/yue_charge');
	}
}