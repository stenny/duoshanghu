<?php
namespace Wx\Controller;
use Think\Controller;
use Think\Model;

class WxpayController extends BaseController
{

    public function _initialize()
    {

        vendor('WxPayPubHelper.WxPayPubHelper');
    }

    public function index()
    {
        $config = M('Payments')->field('payName,payConfig')->where(array('payCode' => 'weixin', 'enabled' => 1))->find();
        $payConfig = json_decode($config['payConfig']);
        if (!$config) {
            $this->error('未开启微信支付');
        }



        $jsApi = new \JsApi_pub($payConfig->appId, $payConfig->mchId, $payConfig->apiKey, $payConfig->appsecret);
//        $jsApi = new \JsApi_pub(C('WxPayConf_pub.APPID'),C('WxPayConf_pub.MCHID'),C('WxPayConf_pub.KEY'),C('WxPayConf_pub.APPSECRET'));
        //=========步骤1：网页授权获取用户openid============
        //通过code获得openid
        if (!isset($_GET['code'])) {
            //触发微信返回code码
            $call_url = C('WxPayConf_pub.JS_API_CALL_URL');
            $call_url .= "?needPay=" . I('get.needPay') . "+&payid=" . I('get.payid') . "+&Nos=" . I('get.Nos');
            $url = $jsApi->createOauthUrlForCode(urlencode($call_url));
            Header("Location: $url");
        } else {
            //获取code码，以获取openid
            $code = $_GET['code'];
            $jsApi->setCode($code);
            $openid = $jsApi->getOpenId();
        }


        $payid = I('get.payid');
        $Nos = I('get.Nos');
        $needPay = I('get.needPay');

        //=========步骤2：使用统一支付接口，获取prepay_id============
        //使用统一支付接口
//        $unifiedOrder = new \UnifiedOrder_pub(C('WxPayConf_pub.APPID'),C('WxPayConf_pub.MCHID'),C('WxPayConf_pub.KEY'),C('WxPayConf_pub.APPSECRET'));

        $unifiedOrder = new \UnifiedOrder_pub($payConfig->appId, $payConfig->mchId, $payConfig->apiKey, $payConfig->appsecret);
         $shopName = M('sys_configs')->where(array('fieldCode'=>'mallTitle'))->getField('fieldValue');
        //设置统一支付接口参数
        //设置必填参数
        //appid已填,商户无需重复填写
        //mch_id已填,商户无需重复填写
        //noncestr已填,商户无需重复填写
        //spbill_create_ip已填,商户无需重复填写
        //sign已填,商户无需重复填写
        $unifiedOrder->setParameter("openid", $openid);//商品描述
        $unifiedOrder->setParameter("body", $shopName.'商品');//商品描述
        //自定义订单号，此处仅作举例
        $timeStamp = time();
        $out_trade_no = $payConfig->appId . '_' . $payid . '_' . rand(1, 1000);
        $unifiedOrder->setParameter("out_trade_no", $out_trade_no);//商户订单号
        $unifiedOrder->setParameter("total_fee", $needPay * 100);//总金额
        $unifiedOrder->setParameter("notify_url", C('WxPayConf_pub.NOTIFY_URL'));//通知地址
        $unifiedOrder->setParameter("trade_type", "JSAPI");//交易类型
        //非必填参数，商户可根据实际情况选填

        $prepay_id = $unifiedOrder->getPrepayId();

        //=========步骤3：使用jsapi调起支付============
        $jsApi->setPrepayId($prepay_id);

        $jsApiParameters = $jsApi->getParameters();


        $this->assign('payid', $payid);
        $this->assign('jsApiParameters', $jsApiParameters);
        session('payid', null);
        $this->display();
    }

    public function wxCallBack()
    {
        D('Wx/Wxpay')->check();

    }


    public function native($payid,$payMoney){



        $config = M('Payments')->field('payName,payConfig')->where(array('payCode' => 'weixin', 'enabled' => 1))->find();
        $payConfig = json_decode($config['payConfig']);
        if (!$config) {
            $this->error('未开启微信支付');
        }
        $unifiedOrder = new \UnifiedOrder_pub($payConfig->appId, $payConfig->mchId, $payConfig->apiKey, $payConfig->appsecret);
        $shopName = M('sys_configs')->where(array('fieldCode'=>'mallTitle'))->getField('fieldValue');



        $unifiedOrder->setParameter("body", $shopName."PC端扫码支付");//商品描述
        //自定义订单号，此处仅作举例
        $timeStamp = time();
        $out_trade_no = $payConfig->appId . '_' . $payid . '_' . rand(1, 1000);
        $unifiedOrder->setParameter("out_trade_no", $out_trade_no);//商户订单号
        $unifiedOrder->setParameter("total_fee", $payMoney*100);//总金额
        $unifiedOrder->setParameter("notify_url", C('WxPayConf_pub.NOTIFY_URL'));//通知地址
        $unifiedOrder->setParameter("trade_type", "NATIVE");//交易类型
        $unifiedOrder->setParameter("product_id", $payid);//交易类型
        //非必填参数，商户可根据实际情况选填

        $res = $unifiedOrder->getResult();
        $res['img'] = D("Wx/wxpay")->qcodeImg($res['code_url']);

        return  $res;
    }

    public function wxNativeCheck(){
//        if(IS_AJAX){
        $res = D('Wx/Wxpay')->wxNativeCheck();
        $this->ajaxReturn($res);
//        }

    }


}