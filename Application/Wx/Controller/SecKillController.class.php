<?php
namespace Wx\Controller;
use Think\Controller;
use Think\Model;
use Wx\Controller\BaseController;
class SecKillController extends BaseController{
    
    public  function index(){
        //20：00-->0:00 时间段秒杀
        $starTime=strtotime(date('Y-m-d').'20:00:00');
        $endTime=strtotime(date('Y-m-d').'23:59:59');


//        $starTime = strtotime(date('Y-m-d'));
//        $endTime=strtotime(date('Y-m-d'));

        $this->oneStarTime=$starTime;
        $this->ondEndTime=$endTime;
        $field="k.shopId,k.goodsId,k.seckillPrice,k.seckillMaxCount,s.goodsName,s.goodsThums,s.marketPrice,s.shopPrice,k.goodsStock";
        $map['k.goodsSeckillStatus']=1;
        $map['k.seckillStatus']=1;
        $map['s.goodsStatus']=1;
        $map['s.goodsFlag']=1;
        $map['s.isSeckill']=1;
        $map['k.seckillSetTime']=4;

//        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");


//        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
//        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");

        $db=M('goods_seckill');
//        $info=$db->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();


        $sql  = "SELECT
  * FROM
  `oto_goods_seckill`
    as k left join oto_goods as s
    on s.goodsId=k.goodsId
WHERE
  ( k.goodsSeckillStatus = 1 ) AND ( k.seckillStatus = 1 )
  AND ( s.goodsStatus = 1 )
  AND ( s.goodsFlag = 1 )
  AND ( s.isSeckill = 1 )
  AND ( k.seckillSetTime = 4 )
and (unix_timestamp() BETWEEN k.seckillStartTime and k.seckillEndTime)";


        $info =  M()->query($sql);

//        dump($info);
//        echo M()->getLastSql();
        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=0;
            }
        }
        $this->one=$info;
        
        //00:00->08:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'00:00:00');
        $endTime=strtotime(date('Y-m-d').'08:00:00');
        $this->twoStarTime=$starTime;
        $this->twoEndTime=$endTime;
//        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
//        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
//        $info=$db->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();


        $sql  = "SELECT
  * FROM
  `oto_goods_seckill`
    as k left join oto_goods as s
    on s.goodsId=k.goodsId
WHERE
  ( k.goodsSeckillStatus = 1 ) AND ( k.seckillStatus = 1 )
  AND ( s.goodsStatus = 1 )
  AND ( s.goodsFlag = 1 )
  AND ( s.isSeckill = 1 )
  AND ( k.seckillSetTime = 0 )
and (unix_timestamp() BETWEEN k.seckillStartTime and k.seckillEndTime)";


        $info =  M()->query($sql);


        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=0;
            }
        }
        $this->two=$info;
        
        //08:00->12:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'08:00:00');
        $endTime=strtotime(date('Y-m-d').'12:00:00');
        $this->threeStarTime=$starTime;
        $this->threeEndTime=$endTime;
//        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
//        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
//       $info=$db->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();


        $sql  = "SELECT
  * FROM
  `oto_goods_seckill`
    as k left join oto_goods as s
    on s.goodsId=k.goodsId
WHERE
  ( k.goodsSeckillStatus = 1 ) AND ( k.seckillStatus = 1 )
  AND ( s.goodsStatus = 1 )
  AND ( s.goodsFlag = 1 )
  AND ( s.isSeckill = 1 )
  AND ( k.seckillSetTime = 1 )
and (unix_timestamp() BETWEEN k.seckillStartTime and k.seckillEndTime)";


        $info =  M()->query($sql);





        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=0;
            }
        }
        $this->three=$info;

        
        
        //12:00->16:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'12:00:00');
        $endTime=strtotime(date('Y-m-d').'16:00:00');
        $this->fourStarTime=$starTime;
        $this->fourEndTime=$endTime;
//        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
//        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
//        $info=$db->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();


        $sql  = "SELECT
  * FROM
  `oto_goods_seckill`
    as k left join oto_goods as s
    on s.goodsId=k.goodsId
WHERE
  ( k.goodsSeckillStatus = 1 ) AND ( k.seckillStatus = 1 )
  AND ( s.goodsStatus = 1 )
  AND ( s.goodsFlag = 1 )
  AND ( s.isSeckill = 1 )
  AND ( k.seckillSetTime = 2 )
and (unix_timestamp() BETWEEN k.seckillStartTime and k.seckillEndTime)";


        $info =  M()->query($sql);




        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');
                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);

                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');

                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;


                $info[$k]['status']=0;
            }
        }
        $this->four=$info;
        
        //16:00->20:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'16:00:00');
        $endTime=strtotime(date('Y-m-d').'20:00:00');

        $this->fiveStarTime=$starTime;
        $this->fiveEndTime=$endTime;
//        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
//        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
//        $info=$db->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();


        $sql  = "SELECT
  * FROM
  `oto_goods_seckill`
    as k left join oto_goods as s
    on s.goodsId=k.goodsId
WHERE
  ( k.goodsSeckillStatus = 1 ) AND ( k.seckillStatus = 1 )
  AND ( s.goodsStatus = 1 )
  AND ( s.goodsFlag = 1 )
  AND ( s.isSeckill = 1 )
  AND ( k.seckillSetTime = 3 )
and (unix_timestamp() BETWEEN k.seckillStartTime and k.seckillEndTime)";


        $info =  M()->query($sql);




        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $seckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$seckillStock)*100;
                $info[$k]['status']=0;
            }
        }
         $this->five=$info;
        
        $this->display();
    }
    public function format($time){

        return  date("Y-m-d H:i:s",$time);

    }
}