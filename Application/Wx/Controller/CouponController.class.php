<?php
namespace Wx\Controller;

use Think\Controller;
use Think\Model;
class CouponController extends BaseController {
    public function _initialize()
    {
        parent::isLogin();
    }
    //领取优惠卷
    public function getCoupon(){
        $time=time();
        $uid=session('oto_userId');
        $user_id=session('oto_userId');
        /*
        $sql="SELECT y.id,y.deal_cate_id,s.shopName,y.deal_cate_id,y.good_id,y.supplier_id,y.shop_cat_id,y.brand_id,y.name,y.youhui_scope,y.good_id,y.supplier_id,y.begin_time,y.end_time,y.youhui_type,y.total_fee,y.breaks_menoy,y.deal_cate_id,y.expire_day,y.city_id FROM `oto_youhui` as y  left join oto_shops as s on y.supplier_id=s.shopId where $time between begin_time and end_time  and y.id not in(select youhui_id from oto_youhui_user_link where user_id=$uid)";
        $mayReceive=M()->query($sql);
        $codition='';
        foreach ($mayReceive as $k=>$v){
            switch($v['youhui_scope']){
                case 1:$codition='全部商品';break;
                case 2:$codition=$v['shopName'];break;
                case 3:$codition=$v['shopName'].'部分商品';break;
                case 4:$codition=$v['shopName'].'指定品牌';break;
                case 5:$codition=M('goods_cats')->where(array('catId'=>$v['deal_cate_id']))->getField('catName');break;
            }
            $mayReceive[$k]['condition']=$codition;
        }
        */
        $sql="select * from `oto_youhui` where end_time>'{$time}' and `begin_time`<='{$time}' and is_effect='1'";
        //file_put_contents("tsxx.txt", "\r\n".$sql."\r\n", FILE_APPEND);
        $youhui=M()->query($sql);
        if(isset($youhui))
        {
            for($i=0;$i<count($youhui);$i++)
            {
                $youhui_id=$youhui[$i]['id'];
                $sql="select * from `oto_youhui_user_link` where youhui_id='{$youhui_id}' and u_is_effect='1' and user_id='{$user_id}'";
                $youhui_user_link[$i]=M()->query($sql);
                $sql="select * from `oto_youhui_use_record` where youhui_id='{$youhui_id}' and userId='{$user_id}'";
                $youhui_use_record[$i]=M()->query($sql);
                $reco=0;
                if(isset($youhui_use_record[$i]))
                {
                    for($j=0;$j<count($youhui_use_record[$i]);$j++)
                    {
                        if(date('Y-m-d',$youhui_use_record[$i][$j]['useTime'])==date('Y-m-d',$time))
                        {
                            $reco++;
                        }
                    }
                }
                if(!isset($youhui_user_link[$i]))
                {
                    $youhui[$i]['youhui_user_link']=$reco;
                }
                else
                {
                    $youhui[$i]['youhui_user_link']=$youhui_user_link[$i][0]['todayget'];
                }
            }
            for($i=0;$i<count($youhui);$i++)
            {
                $shopId=$youhui[$i]['supplier_id'];
                $sql="select shopName,shopId from `oto_shops` where shopId='{$shopId}'";
                $shops[$i]=M()->query($sql);
                if(isset($shops[$i]))
                {
                    $youhui[$i]['shopName']=$shops[$i][0]['shopName'];
                    $youhui[$i]['shopId']=$shops[$i][0]['shopId'];
                }
                else
                {
                    $youhui[$i]['shopName']="";
                    $youhui[$i]['shopId']="";
                }
                if($youhui[$i]['youhui_scope']==1)
                {
                    $youhui[$i]['msg']="全部商品";
                }
                if($youhui[$i]['youhui_scope']==2)
                {
                    $shop_cat_id=$youhui[$i]['shop_cat_id'];
                    $shop_cat_name="";
                    $sql="select catName from `oto_shops_cats` where catId='{$shop_cat_id}'";
                    $shop_cat[$i]=M()->query($sql);
                    if(isset($shop_cat[$i]))
                    {
                        $shop_cat_name=$shop_cat[$i][0]['catName'];
                    }
                    $youhui[$i]['msg']="店铺分类：".$shop_cat_name;
                }
                if($youhui[$i]['youhui_scope']==3)
                {
                    $goods_id=$youhui[$i]['good_id'];
                    $sql="select goodsName from `oto_goods` where goodsId='{$goods_id}'";
                    $goods[$i]=M()->query($sql);
                    if(isset($goods[$i]))
                    {
                        $goodsName=$goods[$i][0]['goodsName'];
                    }
                    else $goodsName="";
                    $youhui[$i]['msg']="商品：".$goodsName;
                }
                if($youhui[$i]['youhui_scope']==4)
                {
                    $brand_id=$youhui[$i]['brand_id'];
                    $brand_name="";
                    $sql="select brandName from `oto_brands` where brandId='{$brand_id}'";
                    $brand[$i]=M()->query($sql);
                    if(isset($brand[$i]))
                    {
                        $brand_name=$brand[$i][0]['brandName'];
                    }
                    $youhui[$i]['msg']="品牌：".$brand_name;
                }
                if($youhui[$i]['youhui_scope']==5)
                {
                    $cat_name="";
                    $cat_id=$youhui[$i]['deal_cate_id'];
                    $sql="select catName from `oto_goods_cats` where catId='{$cat_id}'";
                    $cat[$i]=M()->query($sql);
                    if(isset($cat[$i]))
                    {
                        $cat_name=$cat[$i][0]['catName'];
                    }
                    $youhui[$i]['msg']="商城分类：".$cat_name;
                }
            }
            $num=0;
            $num2=0;
            for($i=0;$i<count($youhui);$i++)
            {
                $s=$youhui[$i]['user_limit']-$youhui[$i]['youhui_user_link'];
                $sy=$youhui[$i]['total_num']-$youhui[$i]['user_count'];
                if($sy>=$s) $s=$s;
                else $s=0;
                $num+=$s;
                for($j=0;$j<$s;$j++)
                {
                    $data[$num2]=$youhui[$i];
                    $num2++;
                }
            }
            if($num2==0) $this->mayReceive=0;
            else $this->mayReceive=$data;
        }
        else $this->mayReceive=0;
        //$this->mayReceive=$mayReceive;
        $this->display();
    }
    //领取优惠卷处理
    public function getCouponHandel(){
        if (!session('oto_userId')) {
         $this->ajaxReturn(array('status'=>-1));
            return;
        }
     
        if(IS_AJAX){
            $id=I('id');
            $uinfo=M('youhui_user_link')->where(array('youhui_id'=>$id,'user_id'=>session('oto_userId')))->find();
            $couponInfo=M('youhui')->where(array('id'=>$id))->find();
            if($uinfo){
                if($uinfo['todayget']>=$couponInfo['user_limit']){
                    $this->ajaxReturn(array('status'=>-3));
                    return;
                }
            }
            if($couponInfo['begin_time']<time()&&$couponInfo['end_time']>time()){
                if($uinfo){
                    $A=M('youhui_user_link')->where(array('youhui_id'=>$id))->setInc('surplus',1);
                    $B=M('youhui_user_link')->where(array('youhui_id'=>$id))->setInc('todayget',1);
                    if($A&&$B){
                        $this->ajaxReturn(array('status'=>0));
                    }else{
                        $this->ajaxReturn(array('status'=>-1));
                    }
                }else{
                    $data['youhui_id']=$id;
                    $data['user_id']=session('oto_userId');
                    $data['surplus']=1;
                    $data['todayget']=1;
                    $data['u_is_effect']=1;
                    $data['get_time']=time();
                    $res=M('youhui_user_link')->add($data);
                    M('Youhui')->where(array('id'=>$id))->setInc('user_count');
                    if($res){
                        $this->ajaxReturn(array('status'=>0));
                    }else{
                        $this->ajaxReturn(array('status'=>-1));
                    }
                }
            }else{
                $this->ajaxReturn(array('status'=>-1));
            }
        }
    }
    
    // 优惠卷处理
    public function coupon(){
        //优惠券刷新
        $time=time();
        $user_id=session('oto_userId');
        $user_id=$_REQUEST['user_id'];
        $time=time();
        $sql="select * from `oto_youhui_user_link`,`oto_youhui` where oto_youhui_user_link.user_id='{$user_id}' and u_is_effect='1' and oto_youhui_user_link.youhui_id=oto_youhui.id";
        $yhq_info=M()->query($sql);
        if(!isset($yhq_info))
        {
            $yhq_info='';
        }
        else
        {
            $t1=strtotime(date('Y-m-d 00:00:00',$time));
            for($i=0;$i<count($yhq_info);$i++)
            {
                $youhui_id=$yhq_info[$i]['youhui_id'];
                if($yhq_info[$i]['end_time']<$time)
                {
                    
                    $sql="update oto_youhui_user_link set u_is_effect='0' where user_id='{$user_id}' and youhui_id='{$youhui_id}'";
                    M()->query($sql);
                }
                if($yhq_info[$i]['get_time']<$t1)
                {
                    $sql="update oto_youhui_user_link set todayget='0' where user_id='{$user_id}' and youhui_id='{$youhui_id}'";
                    M()->query($sql);
                }
            }
        }

        //有多少优惠卷待使用
//        session('oto_userId',59);

        $time=time();
        /*
        $field="y.deal_cate_id,u.surplus,s.shopName,y.deal_cate_id,y.good_id,y.supplier_id,y.shop_cat_id,y.brand_id,y.name,y.youhui_scope,y.good_id,y.supplier_id,y.begin_time,y.end_time,y.youhui_type,y.total_fee,y.breaks_menoy,y.deal_cate_id,y.expire_day,y.city_id";
        $notUse=M('youhui_user_link')->field($field)->where(array('u.user_id'=>session('oto_userId'),'u.surplus'=>array('gt',0)))->join("as u  join oto_youhui as y on u.youhui_id=y.id and  $time between begin_time and end_time left join oto_shops as s on s.shopId=y.supplier_id" )->select();



        foreach ($notUse as $k=>$v){
            switch($v['youhui_scope']){
                case 1:$codition='全部商品';break;
                case 2:$codition=$v['shopName'];break;
                case 3:$codition=$v['shopName'].'部分商品';break;
                case 4:$codition=$v['shopName'].'指定品牌';break;
                case 5:$codition=M('goods_cats')->where(array('catId'=>$v['deal_cate_id']))->getField('catName');break;
            }
            $notUse[$k]['condition']=$codition;
        }
        $this->notUse=$notUse;
        */
        $user_id=session('oto_userId');
        $sql="select * from `oto_youhui_user_link`,`oto_youhui` where oto_youhui_user_link.user_id='{$user_id}' and u_is_effect='1' and oto_youhui_user_link.youhui_id=oto_youhui.id";
        $youhui=M()->query($sql);
        if(isset($youhui))
        {
            for($i=0;$i<count($youhui);$i++)
            {
                $shopId=$youhui[$i]['supplier_id'];
                $sql="select shopName,shopId,shopImg from `oto_shops` where shopId='{$shopId}'";
                $shops[$i]=M()->query($sql);
                if(isset($shops[$i]))
                {
                    $youhui[$i]['shopName']=$shops[$i][0]['shopName'];
                    $youhui[$i]['shopId']=$shops[$i][0]['shopId'];
                    $youhui[$i]['shopImg']=$shops[$i][0]['shopImg'];
                }
                else
                {
                    $youhui[$i]['shopName']="";
                    $youhui[$i]['shopId']="";
                    $youhui[$i]['shopImg']="";
                }
                if($youhui[$i]['youhui_scope']==1)
                {
                    $youhui[$i]['msg']="全部商品";
                }
                if($youhui[$i]['youhui_scope']==2)
                {
                    $shop_cat_id=$youhui[$i]['shop_cat_id'];
                    $shop_cat_name="";
                    $sql="select catName from `oto_shops_cats` where catId='{$shop_cat_id}'";
                    $shop_cat[$i]=M()->query($sql);
                    if(isset($shop_cat[$i]))
                    {
                        $shop_cat_name=$shop_cat[$i][0]['catName'];
                    }
                    $youhui[$i]['msg']="店铺分类：".$shop_cat_name;
                }
                if($youhui[$i]['youhui_scope']==3)
                {
                    $goods_id=$youhui[$i]['good_id'];
                    $sql="select goodsName from `oto_goods` where goodsId='{$goods_id}'";
                    $goods[$i]=M()->query($sql);
                    if(isset($goods[$i]))
                    {
                        $goodsName=$goods[$i][0]['goodsName'];
                    }
                    else $goodsName="";
                    $youhui[$i]['msg']="商品：".$goodsName;
                }
                if($youhui[$i]['youhui_scope']==4)
                {
                    $brand_id=$youhui[$i]['brand_id'];
                    $brand_name="";
                    $sql="select brandName from `oto_brands` where brandId='{$brand_id}'";
                    $brand[$i]=M()->query($sql);
                    if(isset($brand[$i]))
                    {
                        $brand_name=$brand[$i][0]['brandName'];
                    }
                    $youhui[$i]['msg']="品牌：".$brand_name;
                }
                if($youhui[$i]['youhui_scope']==5)
                {
                    $cat_name="";
                    $cat_id=$youhui[$i]['deal_cate_id'];
                    $sql="select catName from `oto_goods_cats` where catId='{$cat_id}'";
                    $cat[$i]=M()->query($sql);
                    if(isset($cat[$i]))
                    {
                        $cat_name=$cat[$i][0]['catName'];
                    }
                    $youhui[$i]['msg']="商城分类：".$cat_name;
                }
            }
            $sum=0;
            for($i=0;$i<count($youhui);$i++)
            {
                $max=$youhui[$i]['surplus'];
                for($j=0;$j<$max;$j++)
                {
                    $notUse[$sum]=$youhui[$i];
                    $sum++;
                }
            }
        }
        $this->notUse=$notUse;

        //总共有多少张可领取的
        $time=time();
        $uid=session('oto_userId');
        $user_id=session('oto_userId');
        /*
        $sql="SELECT y.deal_cate_id,s.shopName,y.deal_cate_id,y.good_id,y.supplier_id,y.shop_cat_id,y.brand_id,y.name,y.youhui_scope,y.good_id,y.supplier_id,y.begin_time,y.end_time,y.youhui_type,y.total_fee,y.breaks_menoy,y.deal_cate_id,y.expire_day,y.city_id FROM `oto_youhui` as y  left join oto_shops as s on y.supplier_id=s.shopId where $time between begin_time and end_time  and y.id not in(select youhui_id from oto_youhui_user_link where user_id=$uid)";
        $mayReceive=M()->query($sql);
        $this->mayReceive=count($mayReceive);
        */
        //杨 新增待领取优惠券计算方式
        $sql="select * from `oto_youhui` where end_time>'{$time}' and `begin_time`<='{$time}' and is_effect='1'";
        //file_put_contents("tsxx.txt", "\r\n".$sql."\r\n", FILE_APPEND);
        $youhui=M()->query($sql);
        if(isset($youhui))
        {
            for($i=0;$i<count($youhui);$i++)
            {
                $youhui_id=$youhui[$i]['id'];
                $sql="select * from `oto_youhui_user_link` where youhui_id='{$youhui_id}' and u_is_effect='1' and user_id='{$user_id}'";
                $youhui_user_link[$i]=M()->query($sql);
                $sql="select * from `oto_youhui_use_record` where youhui_id='{$youhui_id}' and userId='{$user_id}'";
                $youhui_use_record[$i]=M()->query($sql);
                $reco=0;
                if(isset($youhui_use_record[$i]))
                {
                    for($j=0;$j<count($youhui_use_record[$i]);$j++)
                    {
                        if(date('Y-m-d',$youhui_use_record[$i][$j]['useTime'])==date('Y-m-d',$time))
                        {
                            $reco++;
                        }
                    }
                }
                if(!isset($youhui_user_link[$i]))
                {
                    $youhui[$i]['youhui_user_link']=$reco;
                }
                else
                {
                    $youhui[$i]['youhui_user_link']=$youhui_user_link[$i][0]['todayget'];
                }
            }
            for($i=0;$i<count($youhui);$i++)
            {
                $shopId=$youhui[$i]['supplier_id'];
                $sql="select shopName,shopId from `oto_shops` where shopId='{$shopId}'";
                $shops[$i]=M()->query($sql);
                if(isset($shops[$i]))
                {
                    $youhui[$i]['shopName']=$shops[$i][0]['shopName'];
                    $youhui[$i]['shopId']=$shops[$i][0]['shopId'];
                }
                else
                {
                    $youhui[$i]['shopName']="";
                    $youhui[$i]['shopId']="";
                }
                if($youhui[$i]['youhui_scope']==1)
                {
                    $youhui[$i]['msg']="全部商品";
                }
                if($youhui[$i]['youhui_scope']==2)
                {
                    $shop_cat_id=$youhui[$i]['shop_cat_id'];
                    $shop_cat_name="";
                    $sql="select catName from `oto_shops_cats` where catId='{$shop_cat_id}'";
                    $shop_cat[$i]=M()->query($sql);
                    if(isset($shop_cat[$i]))
                    {
                        $shop_cat_name=$shop_cat[$i][0]['catName'];
                    }
                    $youhui[$i]['msg']="店铺分类：".$shop_cat_name;
                }
                if($youhui[$i]['youhui_scope']==3)
                {
                    $goods_id=$youhui[$i]['good_id'];
                    $sql="select goodsName from `oto_goods` where goodsId='{$goods_id}'";
                    $goods[$i]=M()->query($sql);
                    if(isset($goods[$i]))
                    {
                        $goodsName=$goods[$i][0]['goodsName'];
                    }
                    else $goodsName="";
                    $youhui[$i]['msg']="商品：".$goodsName;
                }
                if($youhui[$i]['youhui_scope']==4)
                {
                    $brand_id=$youhui[$i]['brand_id'];
                    $brand_name="";
                    $sql="select brandName from `oto_brands` where brandId='{$brand_id}'";
                    $brand[$i]=M()->query($sql);
                    if(isset($brand[$i]))
                    {
                        $brand_name=$brand[$i][0]['brandName'];
                    }
                    $youhui[$i]['msg']="品牌：".$brand_name;
                }
                if($youhui[$i]['youhui_scope']==5)
                {
                    $cat_name="";
                    $cat_id=$youhui[$i]['deal_cate_id'];
                    $sql="select catName from `oto_goods_cats` where catId='{$cat_id}'";
                    $cat[$i]=M()->query($sql);
                    if(isset($cat[$i]))
                    {
                        $cat_name=$cat[$i][0]['catName'];
                    }
                    $youhui[$i]['msg']="商城分类：".$cat_name;
                }
            }
            $num=0;
            $num2=0;
            for($i=0;$i<count($youhui);$i++)
            {
                $s=$youhui[$i]['user_limit']-$youhui[$i]['youhui_user_link'];
                $sy=$youhui[$i]['total_num']-$youhui[$i]['user_count'];
                if($sy>=$s) $s=$s;
                else $s=0;
                $num+=$s;
                for($j=0;$j<$s;$j++)
                {
                    $data[$num2]=$youhui[$i];
                    $num2++;
                }
            }
            if($num2==0) $this->mayReceive=0;
            else $this->mayReceive=count($data);
        }
        else $this->mayReceive=0;
        
        //过期没使用
        $field="y.deal_cate_id,u.surplus,s.shopName,y.deal_cate_id,y.good_id,y.supplier_id,y.shop_cat_id,y.brand_id,y.name,y.youhui_scope,y.good_id,y.supplier_id,y.begin_time,y.end_time,y.youhui_type,y.total_fee,y.breaks_menoy,y.deal_cate_id,y.expire_day,y.city_id";
        $expireNoUse=M('youhui_user_link')->field($field)->join("as u  join oto_youhui as y on u.youhui_id=y.id and  $time not between begin_time and end_time left join oto_shops as s on s.shopId=y.supplier_id where y.id not in(select youhui_id from oto_youhui_use_record where userId=$uid) and u.user_id= $uid" )->select();

        foreach ($expireNoUse as $k=>$v){
            switch($v['youhui_scope']){
                case 1:$codition='全部商品';break;
                case 2:$codition=$v['shopName'];break;
                case 3:$codition=$v['shopName'].'部分商品';break;
                case 4:$codition=$v['shopName'].'指定品牌';break;
                case 5:$codition=M('goods_cats')->where(array('catId'=>$v['deal_cate_id']))->getField('catName');break;
            }
            $expireNoUse[$k]['condition']=$codition;
        }
        $this->expireNoUse=$expireNoUse;
        
        //已经使用
        $existsSql="SELECT y.deal_cate_id, s.shopName, y.deal_cate_id, y.good_id, y.supplier_id, y.shop_cat_id, y.brand_id, y. NAME, y.youhui_scope, y.good_id, y.supplier_id, y.begin_time, y.end_time, y.youhui_type, y.total_fee, y.breaks_menoy, y.deal_cate_id, y.expire_day, y.city_id FROM `oto_youhui_use_record` AS u JOIN oto_youhui AS y ON u.youhui_id = y.id LEFT JOIN oto_shops AS s ON s.shopId = y.supplier_id WHERE (u.userId = $uid)";
         $exisUse=M()->query($existsSql);
         foreach ($exisUse as $k=>$v){
            switch($v['youhui_scope']){
                case 1:$codition='全部商品';break;
                case 2:$codition=$v['shopName'];break;
                case 3:$codition=$v['shopName'].'部分商品';break;
                case 4:$codition=$v['shopName'].'指定品牌';break;
                case 5:$codition=M('goods_cats')->where(array('catId'=>$v['deal_cate_id']))->getField('catName');break;
            }
            $exisUse[$k]['condition']=$codition;
        }
        $this->exisUse=$exisUse;
        
        $this->display();
    }


    //获取用户优惠券页面内容
    public function getuserlist($type=1){
//        session('oto_userId',59);
        $userid=session('oto_userId');
//        $userid=$_SESSION['oto_mall']['WST_USER']['userId'];
        if ($type==2) {
            $m=M('youhui_use_record');
            //所需字段
            $field=array('oto_youhui.name,oto_youhui_use_record.*');
            //分页
            $where['userId']=$userid;
            $record=$m->join('oto_youhui ON oto_youhui_use_record.youhui_id = oto_youhui.id')->field($field)->limit($Page->firstRow.','.$Page->listRows)->order('oto_youhui_use_record.useTime desc')->where($where)->select();
            foreach ($record as $key => $vo) {
                $shopName=M('shops')->field('shopName')->find($vo['shopId']);
                $record[$key]['shopName']=$shopName['shopName'];
            }
            $data['ture']='ture';
            $data['list']=$record;
            return $data;
            die;
        }
        //ture则已过期内容
        if ($type==3) {
            $where['oto_youhui_user_link.u_is_effect']='0';
        }
        if ($type==1){
            $where['oto_youhui_user_link.u_is_effect']='1';
        }
        $m=M('youhui_user_link');
        $where['oto_youhui_user_link.user_id']=$userid;
        $where['oto_youhui_user_link.surplus']=array('GT',0);
        //分页

        //所需字段
        $field=array('oto_youhui.name,oto_youhui.breaks_menoy,oto_youhui.youhui_type,oto_youhui.supplier_id,oto_youhui.breaks_menoy,oto_youhui.total_fee,oto_youhui_user_link.*,oto_youhui.end_time,oto_youhui.begin_time,oto_youhui.youhui_scope,oto_youhui_user_link.u_is_effect,oto_youhui.is_effect');

        $data['list']=$m->join('oto_youhui ON oto_youhui_user_link.youhui_id = oto_youhui.id')->join('oto_users ON oto_youhui_user_link.user_id = oto_users.userId')->field($field)->where($where)->order('create_time desc')->select();


        //内容数组合并和处理
        for ($i=0; $i < count($data['list']); $i++) {
            $data['list'][$i]['supplier_name']=M('shops')->where('shopId='.$data['list'][$i]['supplier_id'])->field('shopName')->find();
            $data['list'][$i]['supplier_name']=$data['list'][$i]['supplier_name']['shopName'];
            $data['list'][$i]['id']=$data['list'][$i]['youhui_id'];
        }
        $data['user_id']=$userid;

        return $data;
    }
}